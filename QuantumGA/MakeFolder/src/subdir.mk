################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CU_SRCS += \
../src/GpuRoutines.cu \
../src/Las.cu \
../src/QuantumGA.cu \
../src/RevSupport.cu \
../src/complexExtension.cu 

OBJS += \
./src/GpuRoutines.o \
./src/Las.o \
./src/QuantumGA.o \
./src/RevSupport.o \
./src/complexExtension.o 

CU_DEPS += \
./src/GpuRoutines.d \
./src/Las.d \
./src/QuantumGA.d \
./src/RevSupport.d \
./src/complexExtension.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cu
	@echo 'Building file: $<'
	@echo 'Invoking: NVCC Compiler'
	/usr/local/cuda/bin/nvcc -G -g -O0 -gencode arch=compute_35,code=sm_35  -odir "src" -M -o "$(@:%.o=%.d)" "$<"
	/usr/local/cuda/bin/nvcc -G -g -O0 --compile --relocatable-device-code=true -gencode arch=compute_35,code=compute_35 -gencode arch=compute_35,code=sm_35  -x cu -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


