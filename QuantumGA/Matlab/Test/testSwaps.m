%Verify that swap forward is identical to swap back and its inverse
clc;clear
numberOfWires = 4 ;
sz = 2^numberOfWires;
wire = 2;
wire2 = 4;
swaps = eye(sz);
for i=wire:wire2-2
    tem = kron (eye(i),calcSwap())
    [n,k] = size(tem);
    n = log(n)/log(2)
    n = 2 ^ (numberOfWires-n);
    tem = kron (tem,eye(n))
    swaps = swaps*tem;
end
swaps
swapsBack = eye(sz)
i = wire2;
while i>wire+1
    tem = kron (eye(i-2),calcSwap());
    [n,k] = size(tem);
    n = log(n)/log(2);
    n = 2^(numberOfWires-n);
    tem = kron (tem,eye(n));
    swapsBack = swapsBack*tem;
    i = i - 1 ;
end
swapsBack
inv(swapsBack)
swaps-swapsBack