function [J] = absSquared(parameter)
[m,n] = size(parameter);
sz = m*n;
J = eye(m);
for i=1:sz
    J(i) = abs(parameter(i))*abs(parameter(i));
end
end