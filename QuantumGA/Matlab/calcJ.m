function [J] = calcJ(parameter)
J = eye(4);
J = J* exp(-1i * parameter/2);
J(6) = exp(1i * parameter/2);
J(11) = exp(1i * parameter/2);
end