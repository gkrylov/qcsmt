function [J] = calcSwap()
J = eye(4);
J(6) = 0;
J(7) = 1;
J(11) = 0;
J(10) = 1;
end