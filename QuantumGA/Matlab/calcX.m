function [Rx] = calcX(parameter)
Rx =  [cos(parameter/2) -1i*sin(parameter/2); -1i*sin(parameter/2) cos(parameter/2)  ];
end