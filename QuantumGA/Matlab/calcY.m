function [Ry] = calcY(parameter)
Ry = [cos(parameter/2) -sin(parameter/2); sin(parameter/2) cos(parameter/2)  ];
end