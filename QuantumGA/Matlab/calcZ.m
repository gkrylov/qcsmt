function [Rz] = calcZ(parameter)
Rz = [exp(-1i*parameter/2) 0 ; 0 exp(1i*parameter/2)];
end