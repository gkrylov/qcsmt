function res = checkFitness(matrix,target)
[m,n] = size(matrix);
sz = m*n;
res = sqrt((m-abs(trace(target'*matrix)))/m)
end