function  [fitness] = checkingScript()
fileID = fopen('./InputFiles/matlabInterimExport.txt','r');
formatSpec = '%d %d';
sizeA = [2 1];
A = fscanf(fileID,formatSpec,sizeA);
sizeOfChromosome = A(2);
numberOfWires = A(1);
q = 0 ;
format SHORT;
ssize = 2^numberOfWires;
swap = [1 0 0 0; 0 0 1 0; 0 1 0 0; 0 0 0 1];

formatSpec = '%d %d %d %f %f';
sizeA = [5 Inf];
A = fscanf(fileID,formatSpec,sizeA);
fclose(fileID);

Target = load('./InputFiles/matlabRecentTarget.txt');
tempresult = zeros(ssize,ssize,sizeOfChromosome);

result = eye(ssize);
pFile = fopen('./OutputFiles/output.txt','w');

for i = 1:sizeOfChromosome
ind = i;
if mod(numberOfWires,2)==1
    ind = sizeOfChromosome-i+1;
end
wire = A(1,ind);
wire2 = A(2,ind);
axis = A(3,ind);
parameter = A(4,ind);
if (parameter~=-10 ) 
   [n,d] = rat(parameter/pi);

end

if (wire == wire2 )
        if (axis == 0) 
            axis2 = 'X'; 
        elseif (axis == 1 ) 
            axis2 = 'Y'; 
        else
            axis2='Z'; 
        end
			fprintf(pFile, 'R %i %c pi*(%d/%d)\n', wire, axis2, n,d);

		
elseif (parameter == -10)
			fprintf(pFile, 'swap %i %i\n', wire, wire2 );
else
		
		fprintf(pFile, 'J %i %i pi*(%d/%d)\n',wire, wire2, n,d);

end
if (wire == wire2)
   if (axis == 0)
     matrix = calcX(parameter);
   elseif (axis == 1)
     matrix = calcY(parameter);  
   elseif (axis == 2)
     matrix = calcZ(parameter);
   end
       
else 
    if (parameter == -10)
        matrix = swap;
        q = i;
    else
        matrix = calcJ(parameter);
    end  
end

tempresult(:,:,i) = expandWithKron(matrix,wire,wire2,numberOfWires);
disp(i)
disp(tempresult(:,:,i));
result = result*tempresult(:,:,i) ;
end
fclose(pFile);
absSquared(result)
fitness= evaluateFitness(result,Target)
diary off
end