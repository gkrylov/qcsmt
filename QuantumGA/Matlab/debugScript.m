clc;
clear;
delete matleb.txt
diary matleb.txt
diary on

cudaOutput = load('./InputFiles/matlabRecentMatrices.txt');
fileID = fopen('./InputFiles/matlabInterimExport.txt','r');
formatSpec = '%d %d';
sizeA = [2 1];
A = fscanf(fileID,formatSpec,sizeA);
sizeOfChromosome = A(2);
numberOfWires = A(1);
q = 0 ;
format SHORT;
ssize = 2^numberOfWires;
swap = [1 0 0 0; 0 0 1 0; 0 1 0 0; 0 0 0 1];

formatSpec = '%d %d %d %f %f';
sizeA = [5 Inf];
A = fscanf(fileID,formatSpec,sizeA);
fclose(fileID);

Target = load('./InputFiles/matlabRecentTarget.txt');
matlabMatrices = zeros(ssize,ssize,sizeOfChromosome );
cudaMatrices = parseOutputToArray(cudaOutput,ssize,sizeOfChromosome);




matlabResult = eye(ssize);
pFile = fopen('./OutputFiles/output.txt','w');

for i = 1:sizeOfChromosome
wire = A(1,i);
wire2 = A(2,i);
axis = A(3,i);
parameter = A(4,i);
if (parameter~=-10 ) 
   [n,d] = rat(parameter/pi);

end

if (wire == wire2 )
        if (axis == 0) 
            axis2 = 'X'; 
        elseif (axis == 1 ) 
            axis2 = 'Y'; 
        else
            axis2='Z'; 
        end
			fprintf(pFile, 'R %i %c pi*(%d/%d)\n', wire, axis2, n,d);

		
elseif (parameter == -10)
			fprintf(pFile, 'swap %i %i\n', wire, wire2 );
else
		
		fprintf(pFile, 'J %i %i pi*(%d/%d)\n',wire, wire2, n,d);

end
if (wire == wire2)
   if (axis == 0)
     matrix = calcX(parameter);
   elseif (axis == 1)
     matrix = calcY(parameter);  
   elseif (axis == 2)
     matrix = calcZ(parameter);
   end
       
else 
    if (parameter == -10)
        matrix = swap;
        q = i;
    else
        matrix = calcJ(parameter);
    end  
end

matlabMatrices(:,:,i) = expandWithKron(matrix,wire,wire2,numberOfWires);

end
fclose(pFile);

for i = 1 : sizeOfChromosome
disp(matlabMatrices(:,:,i));
if mod(numberOfWires,2)==1
    ind=-i+sizeOfChromosome+1;
else
    ind=i;
end
matlabResult = matlabResult*matlabMatrices(:,:,ind) ;
end


matlabResult = absSquared(matlabResult)
fitness= evaluateFitness(matlabResult,Target)
cudaCalculatedResult = eye(ssize);
for i = 1:sizeOfChromosome
    ind=sizeOfChromosome-i+1;
    cudaCalculatedResult = cudaCalculatedResult * cudaMatrices(:,:,ind);
end
cudaCalculatedResult = absSquared(cudaCalculatedResult)

for i = 1:sizeOfChromosome
    if (sum(abs(sum(abs(real(cudaMatrices(:,:,i))-real(matlabMatrices(:,:,i))+ imag(cudaMatrices(:,:,i))-imag(matlabMatrices(:,:,i))))))>0.001)
        disp ('Error is in ')
        disp(i)
    end
end
diary off
