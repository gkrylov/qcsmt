function res = evaluateFitness(matrix,target)
[m,n] = size(matrix);
sz = m*n;
res = 0;
for i=1:sz
    res =  res +abs( abs(matrix(i))*abs(matrix(i))-abs(target(i))*abs(target(i)));
end
res = 1/(1+res);
end