%The function expands the matrix template to a circuit of desired size,
%using Kronecker product
function [result] = expandWithKron(matrix, wire,wire2,numberOfWires)
if wire == wire2
    result = kron( eye(2^(wire-1)),matrix);
    result = kron(result,eye(2^(numberOfWires-wire2)));
else
    sz = 2^numberOfWires;
    result = eye(sz);
    %Interchange wire1 and wire2 to ensure wire2 has greater value
    if wire>wire2
        wire=wire+wire2;
        wire2 = wire-wire2;
        wire = wire-wire2;
    end
    %Insert swap gates to ensure interaction between neighboring qubits
    swaps = eye(sz);
    for i=wire:wire2-2
        tem = kron (eye(i),calcSwap());
        [n,~] = size(tem);
        n = log(n)/log(2);
        n = 2 ^ (numberOfWires-n);
        tem = kron (tem,eye(n));
        swaps = swaps*tem;
    end
    result = result * swaps;
    tempResult = kron( eye(2^(wire2-2)),matrix);
    n = remainingSize(tempResult,numberOfWires);
    tempResult = kron(tempResult,eye(n));
    result = result*tempResult;
    result = result*swaps;
    
end
end