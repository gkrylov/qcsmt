function [ progResult ] = parseOutputToArray( progOutput,ssize,sizeOfChromosome)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
progResult = zeros(ssize,ssize,sizeOfChromosome );

for k = 1:sizeOfChromosome
    for i=1:ssize
        for j=1:ssize
        realPart = progOutput(i+(k-1)*ssize,(j-1)*2+1);
        imagPart =progOutput (i+(k-1)*ssize,(j-1)*2+2);
        progResult(i,j,k)= complex(realPart,imagPart );
        end
    end

end

end

