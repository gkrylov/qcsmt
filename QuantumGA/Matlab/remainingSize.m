function [ output_args ] = remainingSize(matrix,numberOfWires )
%REMAININGSIZE Calculates remaining size for cronecker expansion
%   Detailed explanation goes here
[n,k] = size(matrix);
n = log(n)/log(2);
n = 2 ^ (numberOfWires-n);
output_args = n;
end

