#include "GpuRoutines.h"
__global__ void eyeD(cuDoubleComplex* memory, int size, int amount) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	while (tid < size * size * amount) {
		memory[tid] = make_cuDoubleComplex(0, 0);
		int ind = tid % (size * size);
		if (ind % size == ind / size)
			memory[tid] = make_cuDoubleComplex(1, 0);

		tid += blockDim.x * gridDim.x;
	}
}
__global__ void constructCircuitWithRepetition(int *circuits,
		curandState* globalState) {
	curandState state;
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	while (id < sizeOfIndividual) {
		state = globalState[id];
		int i;
		// here i represents the circuit segment
		for (i = 0; i < sizeOfPopulation; i++) {
			int whichIndividual = (int) (curand_uniform_double(&state)
					* sizeOfPopulation);
			int whichRotationOrInteraction = (int) (curand_uniform_double(
					&state) * (numberOfWires + interactionTemplatesChose));
			int absoluteIndex = whichRotationOrInteraction*sizeOfIndividual*sizeOfPopulation
					+ whichIndividual*sizeOfIndividual + id;
			int indexInCircuit = i * sizeOfIndividual + id;
			circuits[indexInCircuit] = absoluteIndex;
		}
		globalState[id] = state;
		id += blockDim.x * gridDim.x;
	}

}
//TODO too much threads are launched in here, possible optimization
__global__ void globalExponent(cuDoubleComplex* memory, int size, int amount) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	while (tid < size * size * amount) {
		int ind = tid % (size * size);
		if (ind % size == ind / size)
			memory[tid] = cuCexp(
					cuCdiv(memory[tid], make_cuDoubleComplex(2, 0)));
		tid += blockDim.x * gridDim.x;
	}
}
__global__ void measureAxis(cuDoubleComplex* qutrits, int* axis,
		curandState * globalState) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;

	curandState state;
	double rand;
	double a, b;

	//3 here stands for qutrit
	while (tid < numberOfRotationGates * numberOfMeasurements) {
		int id = tid % (numberOfRotationGates);
		skipahead(tid, globalState + id);
		state = globalState[id];

		rand = curand_uniform_double(&state);

		a = cuCabs(qutrits[3 * id]) * cuCabs(qutrits[3 * id]);

		b = cuCabs(qutrits[3 * id + 1]) * cuCabs(qutrits[3 * id + 1]);

		//	if (id == 3) printf("\nthread id:%i a:%lf b%lf rand:%lf", id,a,a*a+b*b,rand);

		if (rand < a) {
			atomicAdd(axis + id, 0);
			globalState[id] = state;
			tid += blockDim.x * gridDim.x;
			continue;

		}
		if (rand < a + b) {
			atomicAdd(axis + id, 1);
			globalState[id] = state;
			tid += blockDim.x * gridDim.x;
			continue;
		}
		atomicAdd(axis + id, 2);
		globalState[id] = state;
		tid += blockDim.x * gridDim.x;
	}

}

__global__ void normalizeAxis(int* axis) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	while (tid < numberOfRotationGates) {
		double division = ((double) axis[tid]) / numberOfMeasurements;
		axis[tid] = round(division);
		tid += blockDim.x * gridDim.x;
	}
}
void prepareInteractionMatrices(cuDoubleComplex* qubitsD,
		cuDoubleComplex* interactionMatrixMemoryD, int size,
		cublasHandle_t* h) {
	//TODO error might be here
	int i;
	cublasHandle_t handle = *h;
	for (i = 0; i < interactionTemplatesNumber; i++) {
		cublasZscal(handle, size * size, &qubitsD[numberOfRotationGates + i],
				interactionMatrixMemoryD + i * size * size, 1);

	}
	cudaDeviceSynchronize();
	globalExponent<<<blocksPerGrid,threadsPerBlock>>>(interactionMatrixMemoryD,size,interactionTemplatesNumber);
}
__global__ void initSegmentPositionFitness(float* segmentPosFitness,
		curandState* devStates) {
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	curandState state;
	double rand;
	while (id < numberOfSegments) {
		state = devStates[id];
		rand = curand_uniform_double(&state);
		segmentPosFitness[id] = rand;
		devStates[id] = state;
		id += blockDim.x * gridDim.x;
	}
}
__global__ void constructGates(int* axis, cuDoubleComplex* qubits,
		cuDoubleComplex* memory, curandState * globalState) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;

	while (tid < numberOfRotationGates) {
		switch (axis[tid]) {
		case 0:

			memory[tid * 4 + 0] = cuCcos(
					cuCdiv(qubits[tid], make_cuDoubleComplex(2, 0)));
			memory[tid * 4 + 1] = cuCmul(make_cuDoubleComplex(0, -1),
					cuCsin(cuCdiv(qubits[tid], make_cuDoubleComplex(2, 0))));
			memory[tid * 4 + 2] = cuCmul(make_cuDoubleComplex(0, -1),
					cuCsin(cuCdiv(qubits[tid], make_cuDoubleComplex(2, 0))));
			memory[tid * 4 + 3] = cuCcos(
					cuCdiv(qubits[tid], make_cuDoubleComplex(2, 0)));
			break;
		case 1:

			memory[tid * 4 + 0] = cuCcos(
					cuCdiv(qubits[tid], make_cuDoubleComplex(2, 0)));
			memory[tid * 4 + 1] = cuCsin(
					cuCdiv(qubits[tid], make_cuDoubleComplex(-2, 0)));
			memory[tid * 4 + 2] = cuCsin(
					cuCdiv(qubits[tid], make_cuDoubleComplex(2, 0)));
			memory[tid * 4 + 3] = cuCcos(
					cuCdiv(qubits[tid], make_cuDoubleComplex(2, 0)));
			break;

		case 2:

			memory[tid * 4 + 0] = cuCexp(
					cuCmul(make_cuDoubleComplex(0, -1),
							cuCdiv(qubits[tid], make_cuDoubleComplex(2, 0))));
			memory[tid * 4 + 1] = make_cuDoubleComplex(0, 0);
			memory[tid * 4 + 2] = make_cuDoubleComplex(0, 0);
			memory[tid * 4 + 3] = cuCexp(
					cuCmul(make_cuDoubleComplex(0, 1),
							cuCdiv(qubits[tid], make_cuDoubleComplex(2, 0))));
			break;
		}
		tid += blockDim.x * gridDim.x;
	}
}
__global__ void constructInterGates(int* axis, cuDoubleComplex* memory) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;

	while (tid < sizeOfPopulation - 2) {
		if ((tid / numberOfWires) == ((tid + 1) / numberOfWires)) {

			if (axis[tid] == 2 && axis[tid + 1] == 2 && axis[tid + 2] != 2) {
				memory[tid * 4 + 0] = memory[(tid + 1) * 4 + 0];
				memory[tid * 4 + 1] = memory[(tid + 1) * 4 + 1];
				memory[tid * 4 + 2] = memory[(tid + 1) * 4 + 2];
				memory[tid * 4 + 3] = memory[(tid + 1) * 4 + 3];
			}
		}
		tid += blockDim.x * gridDim.x;
	}
}
__global__ void rearrangeMatrices(cuDoubleComplex* PrimitiveGates,
		cuDoubleComplex* Data, cuDoubleComplex* Data2) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	while (tid < numberOfWires * 4 * sizeOfPopulation) {
		/*int block = tid / 4;
		 int fourth = tid /(4*numberOfWires);
		 int other = block%4;
		 other = other*generatedGates*4;
		 int ls = tid%4;
		 int third = other+fourth*4+ls;*/
		int ind = tid / 4 % sizeOfPopulation * 4 + tid / (4 * numberOfWires) * 4
				+ tid % 4;
		Data[ind] = PrimitiveGates[tid];
		Data2[ind] = PrimitiveGates[tid];

		//	printf("%i %i \n",tid,ind);
		tid += gridDim.x * blockDim.x;
	}
}
__global__ void constructSwapGates(cuDoubleComplex* memory, int size) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	while (tid < numberOfWires - 1) {
		memory[tid * size * size + 0] = make_cuDoubleComplex(1, 0);
		memory[tid * size * size + 1] = make_cuDoubleComplex(0, 0);
		memory[tid * size * size + 2] = make_cuDoubleComplex(0, 0);
		memory[tid * size * size + 3] = make_cuDoubleComplex(0, 0);
		memory[tid * size * size + 4] = make_cuDoubleComplex(0, 0);
		memory[tid * size * size + 5] = make_cuDoubleComplex(0, 0);
		memory[tid * size * size + 6] = make_cuDoubleComplex(1, 0);
		memory[tid * size * size + 7] = make_cuDoubleComplex(0, 0);
		memory[tid * size * size + 8] = make_cuDoubleComplex(0, 0);
		memory[tid * size * size + 9] = make_cuDoubleComplex(1, 0);
		memory[tid * size * size + 10] = make_cuDoubleComplex(0, 0);
		memory[tid * size * size + 11] = make_cuDoubleComplex(0, 0);
		memory[tid * size * size + 12] = make_cuDoubleComplex(0, 0);
		memory[tid * size * size + 13] = make_cuDoubleComplex(0, 0);
		memory[tid * size * size + 14] = make_cuDoubleComplex(0, 0);
		memory[tid * size * size + 15] = make_cuDoubleComplex(1, 0);
		tid += blockDim.x * gridDim.x;
	}
}

/*
 cudaDeviceSynchronize();

 int i;
 for (i = 0;i<numberOfWires-1;i++){
 cuDoubleComplex** t;
 t = &Data;
 Data=Data3;
 Data3=*t;
 printf("%i %i \n",tid,ind);
 tid = threadIdx.x + blockIdx.x*blockDim.x;
 int size = (int)pow((double)2, (double) i);
 int nrows = 2;
 int ncols = 2;
 int ofset = 4*generatedGates*i;
 int mcols = size;
 int mrows = size;
 while (tid < ncols*nrows*mrows*mcols*generatedGates){
 int BlockIdx = tid/(nrows*ncols);
 int ThreadIdx = tid%(nrows*ncols);
 int StartOfBlock = (BlockIdx / mcols) * nrows * ncols * mcols + ncols*(BlockIdx%mcols);
 int IndexofC = StartOfBlock+ncols*mcols*(ThreadIdx/ncols)+ThreadIdx%ncols;
 int offset = IndexofC/(nrows*ncols*mrows*mcols);
 int IndexofA = offset*nrows*ncols+ThreadIdx;
 int IndexofB = offset*mrows*mcols+BlockIdx % (mrows*mcols);
 Data3[IndexofC]=cuCmul(Data[IndexofB],Data2[ofset+IndexofA]);
 tid += blockDim.x * gridDim.x;
 }

 */__global__ void kronGPU(cuDoubleComplex *Result, cuDoubleComplex *First,
		int mrows, int mcols, cuDoubleComplex *Second, int nrows, int ncols,
		int sizeOCircuit) {

	int id = threadIdx.x + blockIdx.x * blockDim.x;
	while (id < ncols * nrows * mrows * mcols * sizeOCircuit) {
		int BlockIdx = id / (nrows * ncols);
		int ThreadIdx = id % (nrows * ncols);
		int StartOfBlock = (BlockIdx / mcols) * nrows * ncols * mcols
				+ ncols * (BlockIdx % mcols);
		int IndexofC = StartOfBlock + ncols * mcols * (ThreadIdx / ncols)
				+ ThreadIdx % ncols;
		int offset = IndexofC / (nrows * ncols * mrows * mcols);
		int IndexofA = offset * nrows * ncols + ThreadIdx;
		int IndexofB = offset * mrows * mcols + BlockIdx % (mrows * mcols);
		Result[IndexofC] = cuCmul(First[IndexofB], Second[IndexofA]);

		/*	printf("Block idx %i Thread idx %i StartOfBlock %i \n",BlockIdx, ThreadIdx,StartOfBlock);
		 printf ("%i %i %i \n", IndexofA,IndexofB,IndexofC);*/
		id += blockDim.x * gridDim.x;
	}

}
/*
 //At the current state of the project, this function which does store diagonal elements to
 // a new array named probabilities is unnecessary;
 __global__ void estimateProbabilities(cuDoubleComplex* Data, double *probabilities){
 int id = threadIdx.x + blockIdx.x*blockDim.x;
 int gid = (id/3)*9+4*(id%3);
 double temp;
 while (gid<generatedGates*9){
 printf("%i %i\n",gid,id);
 temp = cuCabs(Data[gid]);
 probabilities[id]=temp*temp;
 id+=blockDim.x * gridDim.x;
 gid = (id/3)*9+4*(id%3);
 }
 }
 */

void updateQutrits(cuDoubleComplex* Qutrits, cuDoubleComplex* Result,
		cuDoubleComplex* evolutionMatrix, const int m, const int n,
		const int k) {

	cublasHandle_t cnpHandle;
	cublasCreate(&cnpHandle);

	int lda = m, ldb = k, ldc = m;
	const cuDoubleComplex alf = make_cuDoubleComplex(1, 0);
	const cuDoubleComplex bet = make_cuDoubleComplex(0, 0);
	const cuDoubleComplex *alpha = &alf;
	const cuDoubleComplex *beta = &bet;
	//printf("\nIn the updateQutrits Function\n");
	for (int i = 0; i < numberOfRotationGates; i++)
		cublasZgemm(cnpHandle, CUBLAS_OP_N, CUBLAS_OP_N, m, n, k, alpha,
				evolutionMatrix + i * 9, lda, Qutrits + i * 3, ldb, beta,
				Result + i * 3, ldc);
	cudaDeviceSynchronize();

	//printf("Done");
	cublasDestroy(cnpHandle);
}
/*
 void projectiveMeasurement(cuDoubleComplex* Data, cuDoubleComplex* Result,cuDoubleComplex* measMatrix,const int m, const int k, const int n){

 cublasHandle_t cnpHandle;
 cublasCreate(&cnpHandle);

 int lda = m, ldb = k, ldc = m;
 const cuDoubleComplex alf = make_cuDoubleComplex(1,0);
 const cuDoubleComplex bet = make_cuDoubleComplex(0, 0);
 const cuDoubleComplex *alpha = &alf;
 const cuDoubleComplex *beta = &bet;
 //printf("\n in the projectiveMeasurement Function\n");
 cublasZgemm(cnpHandle, CUBLAS_OP_C, CUBLAS_OP_C, m, n, k, alpha,Data+0*9, lda,measMatrix, ldb, beta, Result+0*9, ldc);
 cudaDeviceSynchronize();
 cublasZgemm(cnpHandle, CUBLAS_OP_N, CUBLAS_OP_N, m, n, k, alpha,measMatrix, lda,Data+0*9, ldb, beta, Result+1*9, ldc);
 cudaDeviceSynchronize();
 cublasZgemm(cnpHandle, CUBLAS_OP_N, CUBLAS_OP_N, m, n, k, alpha,Result+(0)*9, lda,Result+1*9, ldb, beta, Result+2*9, ldc);
 cudaDeviceSynchronize();
 //printf("hi");
 cublasDestroy(cnpHandle);
 }
 */__global__ void initQubits(cuDoubleComplex* Qubits, curandState* globalState) {
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	curandState state;
	double rand;
	while (id < numberOfSegments) {
		state = globalState[id];
		rand = mutationRange
				* (int) (curand_uniform_double(&state)
						* (2 * mutationRangeCoeff) + 1);
		Qubits[id] = make_cuDoubleComplex(rand, 0);
		//printf("tid is %i\n",id);
		globalState[id] = state;
		id += blockDim.x * gridDim.x;

	}
}

__global__ void updateQubits(cuDoubleComplex* Qubits, float* segmentFitness,
		curandState* globalState) {
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	curandState state;
	double rand;
	int sign;
	while (id < numberOfSegments) {
		float adaptiveCoefficient = 1 - segmentFitness[id];
		state = globalState[id];
		/* NEW 3.0*/
		rand = (curand_uniform_double(&state)) * PI * adaptiveCoefficient;
		double prob2 = (curand_uniform_double(&state) * 2 - 1.0);
		//printf("Tid is %i, probTwo is is %lf\n",id,prob2);
		sign = -1;

		if (prob2 > 0.0) {
			sign = +1;
		};
		rand = rand * sign;

		/* NEW

		 rand = ((int) ((curand_uniform_double(&state) * 2 - 1) * 2 * mutationRangeCoeff)* adaptiveCoefficient) * mutationRange;
		 */

		/* NEW 2.0
		 double prob = (curand_uniform_double(&state));
		 //printf("Tid is %i, prob is is %lf\n",id,prob);
		 if (prob> probabilityOfMutation) {
		 id += blockDim.x * gridDim.x;
		 continue;
		 }
		 int denominator = (int) ((curand_uniform_double(&state)) *mutationRangeCoeff) + 1;
		 sign = -1;
		 double prob2=(curand_uniform_double(&state)*2 - 1.0);
		 //printf("Tid is %i, probTwo is is %lf\n",id,prob2);
		 if  (prob2>0.0){
		 sign = +1;
		 };

		 //printf("Tid is %i, sighn is %i\n",id,sign);
		 rand  = sign * PI/denominator;

		 */
		cuDoubleComplex temp = cuCadd(Qubits[id],
				make_cuDoubleComplex(rand, 0));
		while (cuCreal(temp) < 0) {
			temp = cuCadd(temp, make_cuDoubleComplex(2 * PI, 0));
		}
		while (cuCreal(temp) > 2 * PI) {
			temp = cuCadd(temp, make_cuDoubleComplex(-2 * PI, 0));
		}
		Qubits[id] = temp;
		globalState[id] = state;
		id += blockDim.x * gridDim.x;
	}
}

__global__ void updateSameQubitsEqually(cuDoubleComplex* Qubits,
		float* segmentFitness, curandState* globalState) {
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	curandState state;
	double rand;
	int sign;
	while (id < numberOfSegments) {
		float adaptiveCoefficient = 1 - segmentFitness[id];
		state = globalState[id];
		/* OLD
		 rand = (curand_uniform_double(&state) * 2 - 1)*2*PI / mutationRange * adaptiveCoefficient;
		 */

		/* NEW

		 rand = ((int) ((curand_uniform_double(&state) * 2 - 1) * 2 * mutationRangeCoeff)* adaptiveCoefficient) * mutationRange;
		 */

		/* NEW 2.0*/
		double prob = (curand_uniform_double(&state));
		//printf("Tid is %i, prob is is %lf\n",id,prob);
		if (prob > probabilityOfMutation) {
			id += blockDim.x * gridDim.x;
			continue;
		}
		int denominator = (int) ((curand_uniform_double(&state))
				* mutationRangeCoeff) + 1;
		sign = -1;
		double prob2 = (curand_uniform_double(&state) * 2 - 1.0)
				* adaptiveCoefficient;
		//printf("Tid is %i, probTwo is is %lf\n",id,prob2);
		if (prob2 > 0.0) {
			sign = +1;
		};

		//printf("Tid is %i, sighn is %i\n",id,sign);
		rand = sign * PI / denominator;

		cuDoubleComplex temp = cuCadd(Qubits[id],
				make_cuDoubleComplex(rand, 0));
		while (cuCreal(temp) < 0) {
			temp = cuCadd(temp, make_cuDoubleComplex(2 * PI, 0));
		}
		while (cuCreal(temp) > 2 * PI) {
			temp = cuCadd(temp, make_cuDoubleComplex(-2 * PI, 0));
		}
		Qubits[id] = temp;
		globalState[id] = state;
		id += blockDim.x * gridDim.x;
	}
}

__global__ void constructMatricesForQutritEvolution(QutritOper* Qutrits,
		cuDoubleComplex *Data) {
	double c1, c2, c3, s1, s2, s3;
	double p1, p2, p3, p4, p5;

	int id = threadIdx.x + blockIdx.x * blockDim.x;
	while (id < numberOfRotationGates) {
		c1 = cos(Qutrits[id].t1);
		c2 = cos(Qutrits[id].t2);
		c3 = cos(Qutrits[id].t3);
		s1 = sin(Qutrits[id].t1);
		s2 = sin(Qutrits[id].t2);
		s3 = sin(Qutrits[id].t3);
		p1 = Qutrits[id].p1;
		p2 = Qutrits[id].p2;
		p3 = Qutrits[id].p3;
		p4 = Qutrits[id].p4;
		p5 = Qutrits[id].p5;

		Data[9 * id + 0] = cuCmul(cuCexp(make_cuDoubleComplex(0, p1)),
				make_cuDoubleComplex(c1 * c2, 0));
		Data[9 * id + 1] = cuCmul(cuCexp(make_cuDoubleComplex(0, p3)),
				make_cuDoubleComplex(s1, 0));
		Data[9 * id + 2] = cuCmul(cuCexp(make_cuDoubleComplex(0, p4)),
				make_cuDoubleComplex(c1 * s2, 0));
		Data[9 * id + 3] = cuCsub(
				cuCmul(cuCexp(make_cuDoubleComplex(0, -p4 - p5)),
						make_cuDoubleComplex(s2 * s3, 0)),
				cuCmul(cuCexp(make_cuDoubleComplex(0, p1 + p2 - p3)),
						make_cuDoubleComplex(s1 * c2 * c3, 0)));
		Data[9 * id + 4] = cuCmul(cuCexp(make_cuDoubleComplex(0, p2)),
				make_cuDoubleComplex(c1 * c3, 0));
		Data[9 * id + 5] = cuCsub(
				cuCmul(cuCexp(make_cuDoubleComplex(0, -p1 - p5)),
						make_cuDoubleComplex(-c2 * s3, 0)),
				cuCmul(cuCexp(make_cuDoubleComplex(0, p2 - p3 + p4)),
						make_cuDoubleComplex(s1 * s2 * c3, 0)));
		Data[9 * id + 6] = cuCsub(
				cuCmul(cuCexp(make_cuDoubleComplex(0, -p2 - p4)),
						make_cuDoubleComplex(-s2 * c3, 0)),
				cuCmul(cuCexp(make_cuDoubleComplex(0, p1 - p3 + p5)),
						make_cuDoubleComplex(s1 * c2 * s3, 0)));
		Data[9 * id + 7] = cuCmul(cuCexp(make_cuDoubleComplex(0, p5)),
				make_cuDoubleComplex(c1 * s3, 0));
		Data[9 * id + 8] = cuCsub(
				cuCmul(cuCexp(make_cuDoubleComplex(0, -p1 - p2)),
						make_cuDoubleComplex(c2 * c3, 0)),
				cuCmul(cuCexp(make_cuDoubleComplex(0, -p3 + p4 + p5)),
						make_cuDoubleComplex(s1 * s2 * s3, 0)));

		id += blockDim.x * gridDim.x;
	}
}

__global__ void setup_kernel(curandState * state, unsigned long seed) {
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	while (id < (numberOfSegments)) {
		curand_init(seed, id, 0, &state[id]);
		id += blockDim.x * gridDim.x;
	}
}
__global__ void update_random(unsigned long long n, curandState_t *state) {
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	while (id < (numberOfSegments)) {
		skipahead(n, &(state[id]));
		id += blockDim.x * gridDim.x;
	}
}

__global__ void initQutritOpers(QutritOper* Qutrit, curandState* globalState) {
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	curandState state;

	while (id < numberOfRotationGates) {
		state = globalState[id];
		Qutrit[id].t1 = 0; //curand_uniform_double(&state)*PI/2;
		Qutrit[id].t2 = 0; //curand_uniform_double(&state)*PI/2;
		Qutrit[id].t3 = 0; //curand_uniform_double(&state)*PI/2;
		Qutrit[id].p1 = curand_uniform_double(&state) * 2 * PI;
		Qutrit[id].p2 = curand_uniform_double(&state) * 2 * PI;
		Qutrit[id].p3 = curand_uniform_double(&state) * 2 * PI;
		Qutrit[id].p4 = curand_uniform_double(&state) * 2 * PI;
		Qutrit[id].p5 = curand_uniform_double(&state) * 2 * PI;
		//	printf("%lf %lf %lf %lf %lf %lf %lf %lf\n",Qutrit[id].t1,Qutrit[id].t2,Qutrit[id].t3,Qutrit[id].p1,Qutrit[id].p2,Qutrit[id].p3,Qutrit[id].p4,Qutrit[id].p5);
		globalState[id] = state;
		id += blockDim.x * gridDim.x;
	}

}

__global__ void compareWithPreviuous(float* oldSegmentFitness,
		float* segmentFitness, cuDoubleComplex* oldQubits,
		cuDoubleComplex* oldQutrits, cuDoubleComplex* qubits,
		cuDoubleComplex* qutrits, float* bsolut) {
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	while (id < numberOfSegments) {
		if (oldSegmentFitness[id] > segmentFitness[id]) {
			qubits[id] = oldQubits[id];
			if (id < numberOfRotationGates) {
				qutrits[id * 3 + 0] = oldQutrits[id * 3 + 0];
				qutrits[id * 3 + 1] = oldQutrits[id * 3 + 1];
				qutrits[id * 3 + 2] = oldQutrits[id * 3 + 2];
			}
			segmentFitness[id] = oldSegmentFitness[id];
		}
		id += blockDim.x * gridDim.x;
	}
}
__global__ void modifyQutritOpers(QutritOper* Qutrit, curandState* globalState,
		float* segmentFitness) {
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	curandState state;
	//TODO switch on id modulo
	while (id < numberOfRotationGates) {
		float range = (1.0 - segmentFitness[id]);
		/*	float amp = 10;
		 range*=amp;*/
		state = globalState[id];
		double t1 = 0;
		double t2 = 0;
		double t3 = 0;
		double p1 = 0;
		double p2 = 0;
		double p3 = 0;
		double p4 = 0;
		double p5 = 0;
		double k = curand_uniform_double(&state);
		double w = (curand_uniform_double(&state) * 8);
		int q = (int) w;
		range = range * w;
//		if (id == 5) printf("Two random numbers are %lf %lf \n",k,q);
		if (q == 0)
			t1 += (curand_uniform_double(&state) * 2 - 1) * PI / 2 * range;
		if (q == 1)
			t2 += (curand_uniform_double(&state) * 2 - 1) * PI / 2 * range;
		if (q == 2)
			t3 += (curand_uniform_double(&state) * 2 - 1) * PI / 2 * range;
		if (q == 3)
			p1 += (curand_uniform_double(&state) * 2 - 1) * 2 * PI * range;
		if (q == 4)
			p2 += (curand_uniform_double(&state) * 2 - 1) * 2 * PI * range;
		if (q == 5)
			p3 += (curand_uniform_double(&state) * 2 - 1) * 2 * PI * range;
		if (q == 6)
			p4 += (curand_uniform_double(&state) * 2 - 1) * 2 * PI * range;
		if (q == 7)
			p5 += (curand_uniform_double(&state) * 2 - 1) * 2 * PI * range;
		while (t1 > PI / 2)
			t1 -= PI / 2;
		while (t2 > PI / 2)
			t2 -= PI / 2;
		while (t3 > PI / 2)
			t3 -= PI / 2;
		while (t1 < 0)
			t1 += PI / 2;
		while (t2 < 0)
			t2 += PI / 2;
		while (t3 < 0)
			t3 += PI / 2;
		while (p1 > 2 * PI)
			p1 -= 2 * PI;
		while (p1 < 0)
			p1 += 2 * PI;
		while (p2 > 2 * PI)
			p2 -= 2 * PI;
		while (p2 < 0)
			p2 += 2 * PI;
		while (p3 > 2 * PI)
			p3 -= 2 * PI;
		while (p3 < 0)
			p3 += 2 * PI;
		while (p4 > 2 * PI)
			p4 -= 2 * PI;
		while (p4 < 0)
			p4 += 2 * PI;
		while (p5 > 2 * PI)
			p5 -= 2 * PI;
		while (p5 < 0)
			p5 += 2 * PI;

		Qutrit[id].t1 = t1;
		Qutrit[id].t2 = t2;
		Qutrit[id].t3 = t3;
		Qutrit[id].p1 = p1;
		Qutrit[id].p2 = p2;
		Qutrit[id].p3 = p3;
		Qutrit[id].p4 = p4;
		Qutrit[id].p5 = p5;
		globalState[id] = state;
		//if (id == 5)printf("\nq is %i %lf %lf %lf %lf %lf %lf %lf %lf\n",q,Qutrit[id].t1,Qutrit[id].t2,Qutrit[id].t3,Qutrit[id].p1,Qutrit[id].p2,Qutrit[id].p3,Qutrit[id].p4,Qutrit[id].p5);
		id += blockDim.x * gridDim.x;
	}

}
__global__ void initQutrits(cuDoubleComplex* Qutrits,
		curandState* globalState) {
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	curandState state;
	int repeat = 1;
	double real, imag;
	cuDoubleComplex a, b, c;
	while (id < numberOfRotationGates) {
		state = globalState[id];
		while (repeat) {
			real = curand_uniform_double(&state) * 2 - 1;
			imag = curand_uniform_double(&state) * 2 - 1;
			repeat = 0;
			if (real * real + imag * imag > 1)
				repeat = 1;
		}
		a = make_cuDoubleComplex(real, imag);
		repeat = 1;
		while (repeat) {
			real = curand_uniform_double(&state) * 2 - 1;
			imag = curand_uniform_double(&state) * 2 - 1;
			repeat = 0;
			if (real * real + imag * imag > 1 - cuCabs(a) * cuCabs(a))
				repeat = 1;
		}
		b = make_cuDoubleComplex(real, imag);
		repeat = 1;
		while (repeat) {
			real = curand_uniform_double(&state);
			repeat = 0;
			if (real * real > 1 - cuCabs(a) * cuCabs(a) - cuCabs(b) * cuCabs(b))
				repeat = 1;
			imag = sqrt(
					1 - cuCabs(a) * cuCabs(a) - cuCabs(b) * cuCabs(b)
							- real * real);
		}
		double sign = curand_uniform_double(&state) - 0.5;
		imag = sign / abs(sign) * imag;
		c = make_cuDoubleComplex(real, imag);
		Qutrits[3 * id + 0] = a;
		Qutrits[3 * id + 1] = b;
		Qutrits[3 * id + 2] = c;
		globalState[id] = state;
		id += blockDim.x * gridDim.x;
	}
}
// Here is 2d array of SegmentPosition Fitness
// each thread iterates over segments at given position accumulating their corresponding fitness at given position
//
__global__ void constructCircuit(int *circuits, int* segmentHit, int nRows,
		float* colSumD, float* rowSumD, float* segmentPositionFitness,
		curandState* globalState) {
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	curandState state;
	while (id < sizeOfIndividual * (nRows)) {
		segmentHit[id] = 1;
		id += blockDim.x * gridDim.x;
	}
	id = threadIdx.x + blockIdx.x * blockDim.x;
	while (id < sizeOfIndividual * sizeOfPopulation) {
		state = globalState[id];
		int positionInTheIndividual = id % sizeOfIndividual;
		float sumOfFitnessAtGivenPosition = colSumD[positionInTheIndividual];
		double randomNumber = curand_uniform_double(&state)
				* sumOfFitnessAtGivenPosition;
		//printf("id is %i position in the individual is %i Random number is %lf, sumatGivenpositon is %f\n",id,positionInTheIndividual,randomNumber,sumOfFitnessAtGivenPosition);
		int segmentNumber = 0;
		double accumulatedSumOfFitnesses = 0.0;
		while (segmentNumber < nRows - 1) {
			accumulatedSumOfFitnesses +=
					segmentPositionFitness[positionInTheIndividual
							+ segmentNumber * sizeOfIndividual];
			if (accumulatedSumOfFitnesses >= randomNumber)
				break;
			segmentNumber++;
		}
		circuits[id] = segmentNumber;

		segmentHit[segmentNumber * sizeOfIndividual + positionInTheIndividual]++;
		globalState[id] = state;
		id += blockDim.x * gridDim.x;
	}

}
__global__ void constructCircuitNoRepetition(int *circuits, int* segmentHit,
		int nRows, float* colSumD, float* rowSumD,
		float* segmentPositionFitness, int* segmentAlreadyUsed,
		curandState* globalState) {
	curandState state;
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	while (id < sizeOfIndividual) {
		state = globalState[id];
		int i;
		// here i represents the circuit segment
		for (i = 0; i < sizeOfPopulation; i++) {
			int check = 0;
			while (check == 0) {
				int whichIndividual = (int) (curand_uniform_double(&state)
						* sizeOfPopulation);
				int whichRotationOrInteraction = (int) (curand_uniform_double(
						&state) * (numberOfWires + interactionTemplatesChose));
				int absoluteIndex = whichIndividual * sizeOfIndividual
						* (numberOfWires + interactionTemplatesChose)
						+ whichRotationOrInteraction * sizeOfIndividual + id;
				int indexInCircuit = i * sizeOfIndividual + id;

				if (segmentAlreadyUsed[absoluteIndex] == 0) {
					//printf("tid is %i whichIndividual  is %i whichRotationOrInteraction is %i absolute is %i i is %i  index in circuit is %i\n",id,whichIndividual,whichRotationOrInteraction,absoluteIndex,i,indexInCircuit);
					segmentAlreadyUsed[absoluteIndex]++;
					circuits[indexInCircuit] = absoluteIndex;
					check = 1;

				}

			}
		}
		globalState[id] = state;
		id += blockDim.x * gridDim.x;
	}

}
__global__ void clearFitness(float *tempfitness, float* segmentfitness,
		float* segmentPosfitness, curandState* globalState) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	while (tid < sizeOfPopulation) {
		tempfitness[tid] = 0;
		tid += blockDim.x * gridDim.x;
	}
	tid = threadIdx.x + blockIdx.x * blockDim.x;
	while (tid < numberOfSegments) {
		segmentfitness[tid] = 0;
		tid += blockDim.x * gridDim.x;
	}
	/*tid = threadIdx.x + blockIdx.x * blockDim.x;
	 while (tid < numberOfSegments * sizeOfIndividual) {
	 curandState state = globalState[tid];
	 segmentPosfitness[tid] = (float)curand_uniform_double(&state)/100;
	 tid += blockDim.x * gridDim.x;
	 }
	 */

}

__global__ void evaluateFitnessGPU(cuDoubleComplex * Data,
		cuDoubleComplex * Target, float *tempfitness, int size) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	while (tid < sizeOfPopulation * size * size) {
		atomicAdd(tempfitness + tid / size / size,
				(float) (abs(
						cuCabs(Data[tid]) * cuCabs(Data[tid])
								- cuCabs(Target[tid % (size * size)])
										* cuCabs(Target[tid % (size * size)]))));
		tid += blockDim.x * gridDim.x;
	}
}

void printQm(cuDoubleComplex *M, int nrows, int ncols, int file,
		char* fileName) {
	int i;
	if (file == 0) {

		for (i = 0; i < ncols * nrows; i++) {
			if (i % (ncols) == 0)
				printf("\n");
			printf("%lf ", cuCabs(M[i]) * cuCabs(M[i])); //cuCreal(M[i]));
		}
		printf("\n");

		for (i = 0; i < ncols * nrows; i++) {
			if (i % (ncols) == 0)
				printf("\n");
			printf("%lf + %lf *i ", cuCreal(M[i]), cuCimag(M[i]));
		}
	} else {
		FILE* pFile;
		pFile = fopen(fileName, "a");

		fprintf(pFile, "\n[");
		for (i = 0; i < ncols * nrows; i++) {
			if (i % (ncols) == 0 && i != 0)
				fprintf(pFile, "\n");
			fprintf(pFile, "%.4lf ", cuCabs(M[i]) * cuCabs(M[i]));
		}
		fprintf(pFile, "]\n");
		fprintf(pFile, "\n[");
		for (i = 0; i < ncols * nrows; i++) {
			if (i % (ncols) == 0 && i != 0)
				fprintf(pFile, "\n");
			fprintf(pFile, "%+.4lf %+.4lf *i ", cuCreal(M[i]), cuCimag(M[i]));
			//fprintf(pFile, "%4.0lf  ", cuCreal(M[i]));
		}
		fprintf(pFile, "]\n");

		fclose(pFile);
	}
}
// This function is supposed to calculate fitness of individuals in current generation
// The @param1 (Data) represents the device memory that holds sizeOfCircuit*sizeOfPopulation
// final circuit matrices, each consisting of 4^numberOfWires cuDoubleComplex elements.
// The @param2 (Target) is a device memory that holds the representation of the target matrix
// The @param3 (tempFitness) is a device memory that is used during calculation
// The @param4,@param5,@param6 are equal to @param7 (size) which is equal to 2^numberOfWires
// The @param8 (fitnessD) is a device memory that has sizeOfPopulation fitness values
// The @param9 (fitnessH) is a host memory to hold sizeOfPopulation fitness values
// The @param10 is a cublasHandle
void evaluateFitnessAikaterini(cuDoubleComplex * Data, cuDoubleComplex * Target,
		cuDoubleComplex *tempfitness, const int m, const int k, const int n,
		int size, float* fitnessD, float* fitnessH, cublasHandle_t* h) {
	cublasHandle_t handle = (cublasHandle_t) *h;
	int lda = m, ldb = k, ldc = m;
	const cuDoubleComplex alf = make_cuDoubleComplex(1, 0);
	const cuDoubleComplex bet = make_cuDoubleComplex(0, 0);
	const cuDoubleComplex *alpha = &alf;
	const cuDoubleComplex *beta = &bet;
	int i;
	for (i = 0; i < sizeOfPopulation; i++) {
		cublasZgemm(handle, CUBLAS_OP_C, CUBLAS_OP_N, m, n, k, alpha, Target,
				lda, Data + size * size * i, ldb, beta,
				tempfitness + size * size * i, ldc);
	}

	//TODO think about proper allocation

	cudaDeviceSynchronize();

	int dSize = size * size * sizeOfPopulation;
	cuDoubleComplex hData[dSize];
	cudaMemcpy(hData, tempfitness, dSize * sizeof(cuDoubleComplex),
			cudaMemcpyDeviceToHost);

	double fitness[sizeOfPopulation];
	for (i = 0; i < sizeOfPopulation; i++) {
		cuDoubleComplex tSum = make_cuDoubleComplex(0, 0);
		int j;
		int matrixOffset = i * size * size;
		for (j = 0; j < size * size; j += (size + 1)) {
			tSum = cuCadd(tSum, *(hData + matrixOffset + j));
		}
		fitness[i] = cuCabs(tSum);
	}
/*
	for (i = 0; i < sizeOfPopulation; i++) {
		printQm(hData+i*size*size, size, size, 0, NULL);
		printf("\nThe trace of the matrix is %f\n", fitness[i]);
	}
*/
	for (i = 0; i < sizeOfPopulation; i++)
		fitnessH[i] = 1.0 - (float) sqrt((size * 1.0 - fitness[i]) / (size));
	CUDA_CHECK_RETURN(
			cudaMemcpy(fitnessD,fitnessH ,sizeOfPopulation*sizeof(float),cudaMemcpyHostToDevice));
}

__global__ void evaluateSegmentFitness(float *fitness, int* circuits,
		float* segmentfitness, float *segmentPosfitness) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;

	while (tid < sizeOfPopulation * sizeOfIndividual) {
		//if (circuits[tid]<generatedGates)
		atomicAdd(&segmentfitness[circuits[tid]],
				(float) ((float) fitness[tid / sizeOfIndividual])
						/ (sizeOfIndividual));
		atomicAdd(
				&segmentPosfitness[circuits[tid] * sizeOfIndividual
						+ tid % sizeOfIndividual],
				(float) fitness[tid / sizeOfIndividual] / (sizeOfIndividual));
		//else
		//atomicAdd(&segmentfitness[circuits[tid]],penalty * (float) ((float)fitness[tid/sizeOfIndividual])/(generatedGates+numberOfWires));

		tid += blockDim.x * gridDim.x;
	}
}
__global__ void evaluateSegmentFitnessOnePerGen(float *fitness, int* circuits,
		float* segmentfitness) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;

	while (tid < sizeOfIndividual * sizeOfPopulation) {
		segmentfitness[circuits[tid]] = fitness[tid / sizeOfIndividual];
		tid += blockDim.x * gridDim.x;
	}
}
__global__ void evaluateSegmentFitnessManyPerGen(float *fitness, int* circuits,
		float* segmentfitness) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;

	while (tid < sizeOfIndividual * sizeOfPopulation) {
		if (segmentfitness[circuits[tid]] < fitness[tid / sizeOfIndividual])
			segmentfitness[circuits[tid]] = fitness[tid / sizeOfIndividual];
		tid += blockDim.x * gridDim.x;
	}
}

__global__ void normalizeFSegmentPosFitness(float *segmentPosFitness,
		int* segmentHit) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;

	while (tid < sizeOfIndividual * (numberOfSegments)) {
		//if (circuits[tid]<generatedGates)
		segmentPosFitness[tid] = segmentPosFitness[tid] / segmentHit[tid];

		//else
		//atomicAdd(&segmentfitness[circuits[tid]],penalty * (float) ((float)fitness[tid/sizeOfIndividual])/(generatedGates+numberOfWires));

		tid += blockDim.x * gridDim.x;
	}
}
__global__ void normalize_aux(float* vector, int size, float *sum) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	while (tid < size) {
		vector[tid] = vector[tid] / sum[tid / size];
		tid += blockDim.x * gridDim.x;
	}
}
void sumRowsAndColumnsGPU(int nRows, int nCols, float* segmentPosFitness,
		float* sumRows, float *sumCols, float* sumRowsD, float* sumColsD,
		cublasHandle_t* h) {
	cublasHandle_t handle = (cublasHandle_t) *h;
	int i;
	for (i = 0; i < nRows; i++) {
		cublasSasum(handle, nCols, segmentPosFitness + i * nCols, 1,
				sumRows + i);
	}
	cudaDeviceSynchronize();
	for (i = 0; i < nCols; i++) {
		cublasSasum(handle, nRows, segmentPosFitness + i, nCols, sumCols + i);
	}
	cudaDeviceSynchronize();

	CUDA_CHECK_RETURN(
			cudaMemcpy(sumColsD, sumCols, sizeof(float) * nCols,
					cudaMemcpyHostToDevice));
	CUDA_CHECK_RETURN(
			cudaMemcpy(sumRowsD, sumRows, sizeof(float) * nRows,
					cudaMemcpyHostToDevice));

}

__global__ void maxRowsAndColumnsGPU(int nRows, int nCols,
		float* segmentPosFitness, float* sumRowsD) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;

	while (tid < nRows) {
		float max = segmentPosFitness[tid * sizeOfIndividual];
		for (int i = 1; i < nCols; i++) {
			if (segmentPosFitness[tid * sizeOfIndividual + i] > max)
				max = segmentPosFitness[tid * sizeOfIndividual + i];
		}
		sumRowsD[tid] = max;
		tid += blockDim.x * gridDim.x;
	}
}

/*
 void normalizeSSegmentPosFitness(float *segmentPosFitness ){
 cublasHandle_t handle;
 cublasCreate(&handle);


 int Ncols = sizeOfIndividual;
 int Nrows = generatedGates+shift;


 sum_rows = new float[generatedGates];


 cudaMalloc((void**)&sum_rows_d, sizeof(float)*generatedGates);
 int i;
 for (i= 0; i<Ncols; i++){
 sum_rows[i]= 0;
 }
 for (i = 0 ; i < Ncols; i++){
 cublasSasum(handle,Nrows,segmentPosFitness+i, Ncols, sum_rows+i);
 }
 printf("\nDebug of sum_rows\n");
 for (i= 0; i<Ncols; i++){
 printf("%.12f ",sum_rows[i]);
 }

 printf("\n");
 cudaMemcpy(sum_rows_d,sum_rows  ,Nrows*sizeof(float),cudaMemcpyHostToDevice);
 normalize_aux<<<blocksPerGrid,threadsPerBlock>>>(segmentPosFitness,Ncols*Nrows,sum_rows_d);
 cublasDestroy(handle);
 }
 */__global__ void invertFitness(float *tempfitness, int* solut) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	while (tid < sizeOfPopulation) {
		float a = 1 + tempfitness[tid];
		if (1 / a >= 1 - tol)
			*solut = tid;
		tempfitness[tid] = 1.0 / a;
		tid += blockDim.x * gridDim.x;
	}

}
__global__ void invertFitnessWithBest(float *tempfitness, int* solut,
		float *bsolut, int* bsolutI) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	while (tid < sizeOfPopulation) {
		float a = 1 + tempfitness[tid];

		if (1 / a >= 1 - tol) {
			*solut = tid;
			//	printf("\nTid is %i, tempfitness is %f", tid, 1.0 / a);
		}
		if (1.0 / a > bsolut[0]) {
			bsolut[0] = 1.0 / a;
			bsolutI[0] = tid;
		}
		tempfitness[tid] = 1.0 / a;
		tid += blockDim.x * gridDim.x;
	}

}

__global__ void findBest(float *tempfitness, int* solut, float *bsolut,
		int* bsolutI) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	while (tid < sizeOfPopulation) {
		float a = tempfitness[tid];
		if (a > bsolut[0]) {
			bsolut[0] = a;
			bsolutI[0] = tid;
		}
		tid += blockDim.x * gridDim.x;
	}

}

__global__ void selection(float *fitness, float* sum, int* parents,
		curandState* globalState) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	curandState state;
	while (tid < sizeOfPopulation) {
		state = globalState[tid];
		double r = curand_uniform_double(&state) * (*sum);
		float acc = 0;
		int i = 0;
		while (acc < r && i < sizeOfPopulation - 1) {
			acc += fitness[i];
			i++;
		}
		parents[tid] = i;
		globalState[tid] = state;
		tid += blockDim.x * gridDim.x;
	}

}

void gpu_blas_mmul(cuDoubleComplex* temp_circuit,
		cuDoubleComplex* final_circuit, cuDoubleComplex* Gates, int* circuits,
		const int m, const int k, const int n, cublasHandle_t handle, int size,
		cudaStream_t * streams) {

	int lda = m, ldb = k, ldc = m;
	int i, j;

	const cuDoubleComplex alf = make_cuDoubleComplex(1, 0);
	const cuDoubleComplex bet = make_cuDoubleComplex(0, 0);
	const cuDoubleComplex *alpha = &alf;
	const cuDoubleComplex *beta = &bet;
	cuDoubleComplex *t_t;

	for (i = 1; i < sizeOfIndividual; i++) {
		// Launch each DGEMM operation in own CUDA stream
		for (j = 0; j < sizeOfPopulation; j++) {
			// Set CUDA stream
			//	cublasSetStream(handle, streams[j]);
			//	printf("%i %i lala %i\n",i,j,j*sizeOfIndividual+i);
			// ZGEMM: C = alpha*A*B + beta*C
			cublasZgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, m, n, k, alpha,
					final_circuit + size * size * j, lda,
					Gates + size * size * circuits[j * sizeOfIndividual + i],
					ldb, beta, temp_circuit + size * size * j, ldc);
		}
		cudaDeviceSynchronize();
		t_t = final_circuit;
		final_circuit = temp_circuit;
		temp_circuit = t_t;
	}

}

void replication(int *parents, cuDoubleComplex *Qubits,
		cuDoubleComplex *Qubitst, cuDoubleComplex *Qutrits,
		cuDoubleComplex *Qutritst) {
	int j;

	printf("Parents are:\n");
	for (int i = 0; i < sizeOfPopulation; i++) {
		printf("%i ", parents[i]);
	}
	printf("\n");

	for (j = 0; j < sizeOfPopulation; j++) {
		//	printf("J IS FINE %i, parents of[j] is %i \n",j,parents[j]);

		CUDA_CHECK_RETURN(
				cudaMemcpy(Qutritst+j*numberOfWires*3, Qutrits + parents[j]*numberOfWires*3 ,numberOfWires*3*sizeof(cuDoubleComplex),cudaMemcpyDeviceToDevice));
		CUDA_CHECK_RETURN(
				cudaMemcpy(Qubitst +j*numberOfWires, Qubits + parents[j]*numberOfWires ,numberOfWires*sizeof(cuDoubleComplex),cudaMemcpyDeviceToDevice));
	}
	cuDoubleComplex *p, *q;
	p = Qutritst;
	Qutritst = Qutrits;
	Qutrits = p;
	q = Qubitst;
	Qubitst = Qubits;
	Qubits = q;

}
