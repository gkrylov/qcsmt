#ifndef GPUROUTINES_H_
#define GPUROUTINES_H_
#include "parameters.h"
#include "complexExtension.h"

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuComplex.h>
#include <cublas_v2.h>
#include <curand_kernel.h>
#include <stdio.h>
#include <cuComplex.h>
#include "Las.h"

__global__ void kronGPU(cuDoubleComplex*, cuDoubleComplex*, int, int,
		cuDoubleComplex*, int, int, int);
__global__ void setup_kernel(curandState*, unsigned long);
__global__ void initQutritOpers(QutritOper*, curandState*);
__global__ void modifyQutritOpers(QutritOper*, curandState*, float*);
__global__ void initQutrits(cuDoubleComplex*, curandState*);
__global__ void estimateProbabilities(cuDoubleComplex*, double*);
__global__ void constructBlockOfGates(cuDoubleComplex*, cuDoubleComplex*,
		cuDoubleComplex*, cuDoubleComplex*);
__global__ void constructMatricesForQutritEvolution(QutritOper*,
		cuDoubleComplex*);
__global__ void measureAxis(cuDoubleComplex*, int*, curandState*);
__global__ void initQubits(cuDoubleComplex*, curandState*);
__global__ void updateQubits(cuDoubleComplex* Qubits, float*,
		curandState* globalState);
__global__ void rearrangeMatrices(cuDoubleComplex*, cuDoubleComplex*,
		cuDoubleComplex*);
__global__ void constructGates(int*, cuDoubleComplex*, cuDoubleComplex*,
		curandState*);
__global__ void constructInterGates(int*, cuDoubleComplex*);
__global__ void constructCircuit(int *circuits, int* segmentHit, int nRows,
		float* colSum, float* rowSum, float* segmentPositionFitnessD,
		curandState* globalState);
__global__ void constructCircuitNoRepetition(int *circuits, int* segmentHit,
		int nRows, float* colSumD, float* rowSumD,
		float* segmentPositionFitness, int *segmentAlreadyUsed,
		curandState* globalState);
__global__ void compareWithPreviuous(float*, float*, cuDoubleComplex*,
		cuDoubleComplex*, cuDoubleComplex*, cuDoubleComplex*, float*);
__global__ void constructSwapGates(cuDoubleComplex*, int);
__global__ void update_random(unsigned long long n, curandState_t *state);
__global__ void constructCircuitWithRepetition(int *circuits,
		curandState* globalState);
void updateQutrits(cuDoubleComplex*, cuDoubleComplex*, cuDoubleComplex*,
		const int, const int, const int);
void projectiveMeasurement(cuDoubleComplex*, cuDoubleComplex*, cuDoubleComplex*,
		const int, const int, const int);
void multiply(cuDoubleComplex*, cuDoubleComplex*, cuDoubleComplex*, const int,
		const int, const int, cublasHandle_t);
void gpu_blas_mmul(cuDoubleComplex*, cuDoubleComplex*, cuDoubleComplex*, int*,
		const int, const int, const int, cublasHandle_t, int, cudaStream_t*);
//__global__ void evaluateSegmentFitness(float*,int*,float*);
__global__ void constructCircuit(int*, float*, float*, float*, float*,
		curandState*);
__global__ void evaluateSegmentFitness(float *fitness, int* circuits,
		float *segmposfitness, float* segmentfitness);
__global__ void evaluateSegmentFitnessOnePerGen(float *fitness, int* circuits,
		float* segmentfitness);
__global__ void evaluateSegmentFitnessManyPerGen(float *fitness, int* circuits,
		float* segmentfitness);
__global__ void normalizeFSegmentPosFitness(float *segmentPosFitness,
		int* segmentHit);
__global__ void normalize_aux(float* vector, int size, float *sum);
__global__ void normalizeAxis(int* axis);
__global__ void eyeD(cuDoubleComplex* memory, int size, int amount);
void evaluateFitnessAikaterini(cuDoubleComplex * Data, cuDoubleComplex * Target,
		cuDoubleComplex *tempfitness, const int m, const int k, const int n,
		int size, float* fitnessD, float* fitnessH, cublasHandle_t* h);
void sumRowsAndColumnsGPU(int nRows, int nCols, float* segmentPosFitness,
		float* sumRows, float *sumCols, float* sumRowsD, float* sumColsD,
		cublasHandle_t* h);
//void normalizeSSegmentPosFitness(float *segmentPosFitness);
__global__ void evaluateFitnessGPU(cuDoubleComplex *, cuDoubleComplex *, float*,
		int);
__global__ void clearFitness(float *, float*, float*, curandState*);
__global__ void invertFitness(float *, int*);
__global__ void invertFitnessWithBest(float *tempfitness, int* solut,
		float *bsolut, int* bsolutI);
__global__ void findBest(float *tempfitness, int* solut, float *bsolut,
		int* bsolutI);
__global__ void initSegmentPositionFitness(float*, curandState*);
__global__ void selection(float*, float*, int*, curandState*);
void replication(int *parents, cuDoubleComplex *Qubits,
		cuDoubleComplex *Qubitst, cuDoubleComplex *Qutrits,
		cuDoubleComplex *Qutritst);
void prepareInteractionMatrices(cuDoubleComplex*, cuDoubleComplex*, int,
		cublasHandle_t*);
// A wrapper function to calculate fitness
void calculateFitness();
__global__ void maxRowsAndColumnsGPU(int nRows, int nCols,
		float* segmentPosFitness, float* sumRowsD);
#endif /* GPUROUTINES_H_ */
