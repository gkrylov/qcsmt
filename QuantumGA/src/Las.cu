/*
 * transpose.c
 *
 *  Created on: 23 ���. 2015 �.
 *      Author: Georgiy
 */
#include "Las.h"
#include <cuComplex.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <stdio.h>
#include <cublas_v2.h>
#include "parameters.h"

void transpose(cuDoubleComplex *A, int nrows, int ncols) {
	int i = 0;
	int j = 0;
	cuDoubleComplex buff;
	cuDoubleComplex buff2;
	for (i = 0; i < ncols; i++) {
		for (j = i + 1; j < nrows; j++) {
			buff = A[j * ncols + i];
			buff2 = A[i * ncols + j];
			A[j * ncols + i] = buff2;
			A[i * ncols + j] = buff;
		}
	}
}

void eye(cuDoubleComplex* t, int size) {
	int i;
	for (i = 0; i < size * size; i++) {
		if (i % size == i / size)
			t[i] = make_cuDoubleComplex(1, 0);
		else
			t[i] = make_cuDoubleComplex(0, 0);
	}
}

void expandWithKron(char* prefix, cuDoubleComplex* result, cuDoubleComplex* matrix, int mRows, int mCols, int wire1,
		int wire2, int circuitSize) {
	if (prefix != NULL)
		printf("%sStarting expandWithKron mrows=%i, mcols=%i,wire1=%i,wire2=%i, circuitSize=%i\n", prefix, mRows, mCols,
				wire1, wire2, circuitSize);
	if (wire1 < 1 || wire2 > circuitSize || circuitSize < 2) {
		fprintf(stderr, "\nKronecker product arguments are invalid %i, %i, %i\n", wire1, wire2, circuitSize);
		return;
	}

	int size = pow(2, circuitSize);
	int firstMatrixSize = pow(2, wire1 - 1);
	int thirdMatrixSize = pow(2, circuitSize - wire2);
	int secondMatrixSize = pow(2, wire1 - 1) * mRows;
	if (firstMatrixSize < 1 || (secondMatrixSize * thirdMatrixSize > size)) {
		fprintf(stderr,
				"\nKronecker product arguments are invalid firstMatrixSize =%i, secondMatrixSize = %i thirdMatrixSize =%i\n",
				firstMatrixSize, secondMatrixSize, thirdMatrixSize);
		return;
	}
	cuDoubleComplex* firstMatrix = new cuDoubleComplex[firstMatrixSize * firstMatrixSize];
	cuDoubleComplex* secondMatrix = new cuDoubleComplex[secondMatrixSize * secondMatrixSize];
	cuDoubleComplex* thirdMatrix = new cuDoubleComplex[thirdMatrixSize * thirdMatrixSize];
	eye(firstMatrix, firstMatrixSize);
	eye(secondMatrix, secondMatrixSize);
	eye(thirdMatrix, thirdMatrixSize);
	Kronecker_CProduct(secondMatrix, firstMatrix, firstMatrixSize, firstMatrixSize, matrix, mRows, mCols);
	Kronecker_CProduct(result, secondMatrix, secondMatrixSize, secondMatrixSize, thirdMatrix, thirdMatrixSize,
			thirdMatrixSize);

	delete[] firstMatrix;
	delete[] secondMatrix;
	delete[] thirdMatrix;

}

void complexConj(cuDoubleComplex *A, int nrows, int ncols) {
	int i = 0;
	int j = 0;
	cuDoubleComplex buff;
	for (i = 0; i < nrows; i++) {
		for (j = 0; j < ncols; j++) {
			buff = A[i * ncols + j];
			if (cuCreal(buff) == 0 && cuCimag(buff) == 0)
				continue;
			A[i * ncols + j] = make_cuDoubleComplex(cuCreal(buff), -cuCimag(buff));
		}
	}
}

void Kronecker_CProduct(cuDoubleComplex *C, cuDoubleComplex *A, int nrows, int ncols, cuDoubleComplex *B, int mrows,
		int mcols) {
	int ccols, i, j, k, l;
	int block_increment;
	cuDoubleComplex *pB;
	cuDoubleComplex *pC, *p_C;
	ccols = ncols * mcols;
	block_increment = mrows * ccols;
	for (i = 0; i < nrows; C += block_increment, i++)
		for (p_C = C, j = 0; j < ncols; p_C += mcols, A++, j++)
			for (pC = p_C, pB = B, k = 0; k < mrows; pC += ccols, k++)
				for (l = 0; l < mcols; pB++, l++)
					*(pC + l) = cuCmul(*A, *pB);

}

void multiplyC(cuDoubleComplex *result, cuDoubleComplex *mat1, int nrows, int ncols, cuDoubleComplex *mat2, int mrows,
		int mcols) {
	int y, x, i;
	for (int i = 0; i < nrows * mcols; i++) {
		result[i] = make_cuDoubleComplex(0, 0);
	}
	for (y = 0; y < nrows; y++) {
		for (x = 0; x < mcols; x++) {
			for (i = 0; i < ncols; i++) {
				result[y * mcols + x] = cuCadd(result[y * mcols + x], cuCmul(mat1[y * ncols + i], mat2[i * mcols + x]));
				//	printf("%i%i%i\n",i,x,y);
			}
		}
	}
}

void copyArray(cuDoubleComplex *copyTo, cuDoubleComplex *copyFrom, int nrows, int ncols) {
	int i;
	for (i = 0; i < nrows * ncols; i++) {
		copyTo[i] = copyFrom[i];
		//printf("copyArray: i = %i, copyFrom[%i] = %lf, copyTo[%i] = %lf\n", i,i,cuCabs(copyFrom[i]),i,cuCabs(copyTo[i]));
	}
}
int getSizeOfDoubleComplexArray(cuDoubleComplex* memory) {
	cuDoubleComplex* size = new cuDoubleComplex;
	cuDoubleComplex *tpointer = memory - 1;
	size[0] = tpointer[0];
	return (int) cuCreal(size[0]);
}
int getSizeOfDoubleComplexArrayDevice(cuDoubleComplex* memory) {
	cuDoubleComplex* size = new cuDoubleComplex;
	cuDoubleComplex *tpointer = memory - 1;
	cudaMemcpy(size, tpointer, sizeof(cuDoubleComplex), cudaMemcpyDeviceToHost);
	return (int) cuCreal(size[0]);
}
int getSizeOfFloatArrayDevice(float* memory) {
	float* size = new float;
	float *tpointer = memory - 1;

	cudaMemcpy(size, tpointer, sizeof(float), cudaMemcpyDeviceToHost);
	return (int) size[0];
}
int getSizeOfIntArrayDevice(int* memory) {
	int* size = new int;
	int *tpointer = memory - 1;
	cudaMemcpy(size, tpointer, sizeof(int), cudaMemcpyDeviceToHost);
	return size[0];
}
int getSizeOfFloatArray(float* memory) {
	memory = memory-1;

	return (int) memory[0];
}
int getSizeOfIntArray(int* memory) {
	memory = memory-1;

	return memory[0];
}
cuDoubleComplex* allocateDoubleComplexArray(cuDoubleComplex** memory, int size) {
	*memory = new cuDoubleComplex[size + 1];
	*memory[0] = make_cuDoubleComplex(size, 0);
	cuDoubleComplex* tempPointer = (*memory) + 1;
	return tempPointer;
}
cuDoubleComplex* allocateDoubleComplexArrayDevice(cuDoubleComplex** memory, int size) {
	CUDA_CHECK_RETURN(cudaMalloc((void ** )memory, (size + 1) * sizeof(cuDoubleComplex)));
	cuDoubleComplex sizeComplex = make_cuDoubleComplex(size, 0);
	cudaMemcpy(*memory, &sizeComplex, sizeof(cuDoubleComplex), cudaMemcpyHostToDevice);
	cuDoubleComplex* tempPointer = (*memory) + 1;
	return tempPointer;
}

float* allocateFloatArrayDevice(float** memory, int size) {
	CUDA_CHECK_RETURN(cudaMalloc((void ** )memory, (size + 1) * sizeof(float)));
	float sizeComplex = (float) size;
	cudaMemcpy(*memory, &sizeComplex, sizeof(float), cudaMemcpyHostToDevice);
	float* tempPointer = (*memory) + 1;
	return tempPointer;
}
int* allocateIntArrayDevice(int** memory, int size) {
	CUDA_CHECK_RETURN(cudaMalloc((void ** )memory, (size + 1) * sizeof(int)));
	int thisSize = size;
	cudaMemcpy(*memory, &thisSize, sizeof(int), cudaMemcpyHostToDevice);
	int* tempPointer = (*memory) + 1;
	return tempPointer;
}
int* allocateIntArray(int** memory, int size) {
	*memory = new int[size + 1];
	*memory[0] = size;
	int* tempPointer = (*memory) + 1;
	return tempPointer;
}

float* allocateFloatArray(float** memory, int size) {
	*memory = new float[size + 1];
	*memory[0] = (float) size;
	float* tempPointer = (*memory) + 1;
	return tempPointer;
}

void deallocateDoubleComplexArray(cuDoubleComplex* memory) {
	memory--;
	delete[] memory;
}
int getDoubleComplexArraySize(cuDoubleComplex* memory) {
	memory--;
	return (int) cuCreal(memory[0]);
}

