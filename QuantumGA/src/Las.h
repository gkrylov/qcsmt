/*
 * Las.h
 *
 *  Created on: Feb 23, 2016
 *      Author: root
 */

#ifndef LAS_H_
#define LAS_H_
/*
 * transpose.h
 *
 *  Created on: 23 ���. 2015 �.
 *      Author: Georgiy
 */
#include <cuComplex.h>
#include <cublas_v2.h>
#include <cuda_runtime_api.h>
#include <cuda.h>
void transpose(cuDoubleComplex * A, int nrows, int ncols);
void Kronecker_CProduct(cuDoubleComplex * C, cuDoubleComplex *A, int nrows, int ncols, cuDoubleComplex *B, int mrows,
		int mcols);
void complexConj(cuDoubleComplex *A, int nrows, int ncols);
void multiplyC(cuDoubleComplex *C, cuDoubleComplex *A, int nrows, int ncols, cuDoubleComplex *B, int mrows, int mcols);
void eye(cuDoubleComplex* memory, int size);
void expandWithKron(char* prefix, cuDoubleComplex* result, cuDoubleComplex* matrix, int mRows, int mCols, int wire1,
		int wire2, int circuitSize);
void copyArray(cuDoubleComplex*, cuDoubleComplex*, int, int);
//These functions allocate one more cell in the memory to store the original size, useful for debug purposes
cuDoubleComplex* allocateDoubleComplexArray(cuDoubleComplex**, int);
cuDoubleComplex* allocateDoubleComplexArrayDevice(cuDoubleComplex**, int);
int getSizeOfDoubleComplexArray(cuDoubleComplex* memory);
int getSizeOfDoubleComplexArrayDevice(cuDoubleComplex* memory);
int* allocateIntArray(int** memory, int size);
int* allocateIntArrayDevice(int** memory, int size);
float* allocateFloatArray(float** memory, int size);
float* allocateFloatArrayDevice(float** memory, int size);
int getSizeOfFloatArrayDevice(float* memory);
int getSizeOfFloatArray(float* memory) ;
int getSizeOfIntArrayDevice(int* memory);
int getSizeOfIntArray(int* memory) ;
void deallocateDoubleComplexArray(cuDoubleComplex*, int);
void deallocateDoubleComplexArrayDevice(cuDoubleComplex*, int);
#endif /* LAS_H_ */
