#include <iostream>
#include <numeric>
#include <stdlib.h>
#include "cublas_v2.h"
#include "GpuRoutines.h"
#include "parameters.h"
#include "Las.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <thrust/extrema.h>
#include <thrust/device_ptr.h>
#include "RevSupport.h"
int i;
struct stat st = { 0 };

curandState* devStates;

cudaStream_t* createStreams(int numberOfStreams) {
	cudaStream_t *streams = (cudaStream_t *) malloc(sizeOfPopulation * sizeof(cudaStream_t));
	int j;
	for (j = 0; j < numberOfStreams; j++) {
		cudaStreamCreate(&streams[j]);
	}
	return streams;
}
void printParameters(int file, char* fileName) {
	if (file == 0) {
		printf("Size of population is %i, Size of chromosome is %i\n", sizeOfPopulation, sizeOfIndividual);
		if (elitism == 1)
			printf("Elitism is enabled \n");
		else
			printf("Elitism is disabled \n");
		printf(
				"Number of Wires is %i , number of measurements is %i, Number of circuits sampled between mutations is %i \n",
				numberOfWires, numberOfMeasurements, numberOfSampledCircuits);
		printf("Probability of mutation is %f, number of rotation gates is %i , number of interaction gates is %i \n",
				probabilityOfMutation, numberOfRotationGates, interactionTemplatesNumber);
		printf("Number of interaction templates is %i, mutationRangeCoeff is %i, mutationRange is %f\n",
				interactionTemplatesChose, mutationRangeCoeff, mutationRange);
		printf("Error tolerance is %f\n", tol);
	} else {
		FILE * pFile;
		pFile = fopen(fileName, "a");
		fprintf(pFile, "Size of population is %i, Size of chromosome is %i\n", sizeOfPopulation, sizeOfIndividual);
		if (elitism == 1)
			fprintf(pFile, "Elitism is enabled \n");
		else
			fprintf(pFile, "Elitism is disabled \n");
		fprintf(pFile,
				"Number of Wires is %i , number of measurements is %i, Number of circuits sampled between mutations is %i, sizeOfCircuit is %i \n",
				numberOfWires, numberOfMeasurements, numberOfSampledCircuits, sizeOfIndividual);
		fprintf(pFile,
				"Probability of mutation is %f, number of rotation gates is %i , number of interaction gates is %i \n",
				probabilityOfMutation, numberOfRotationGates, interactionTemplatesNumber);
		fprintf(pFile, "Number of interaction templates is %i, mutationRangeCoeff is %i, mutationRange is %f\n",
				interactionTemplatesChose, mutationRangeCoeff, mutationRange);
		fprintf(pFile, "Error tolerance is %f\n", tol);
		fprintf(pFile, "\n");
		fclose(pFile);
	}
}
int subIter;
int *axis, *circuits, *circuits_h, *axis_h, *parents, *parents_h;
cuDoubleComplex* Qutrits, *oldQutrits, *Qutrits_h, *final_circuit, *temp_circuit, *final_circuit_h;
char* timeStamp, *fileName, *dirName, *sqFileName, *complexFileName;
cuDoubleComplex *Data, *Data2, *Data3, *Data_h;
cuDoubleComplex *Qutritst;
cuDoubleComplex *interactionMatricesMemory,*tempfitness, *interactionMatricesMemoryD;
cuDoubleComplex *target, *target_h;
float *fitness, *fitness_h, *segmentfitness, *oldSegmentFitness, *segmentfitness_h;
cuDoubleComplex *MatricesForQutritsEvolution;
QutritOper* QutritOpers;
float *sumOfFitness, *sumOfFitness_h;
float *sumRows, *sumRowsD;
float *sumCols, *sumColsD;
cuDoubleComplex *identitym, *identitymD;
int* solut, *solut_d, *bsolutI, *bsolutID;
int* segmentAlreadyUsed, *segmentAlreadyUsedD;
float *bsolut, *bsolutD;
float *sumSegmentRows, *sumSegmentCols, *sumSegmentColsD, *sumSegmentRowsD;
cuDoubleComplex *Qubits, *oldQubits, *Qubitst, *Qubits_h;
cuDoubleComplex *gateMatrices, *gateMatrices_h, matMulRes, matMulRest;

// Segment hit matrix shows how good this segment with respect to its position,
// initially is populated by where the segment is placed to some circuit
// should somehow be modified to drive the evolution
int *segmentHit, *segmentHit_d;
float *segmPosfitness, *segmPosfitness_d;

//Support
cuDoubleComplex *T;
cuDoubleComplex *W;
cuDoubleComplex *E;
cuDoubleComplex *A;
cuDoubleComplex *B;

void printm(cuDoubleComplex *M, int nrows, int ncols, int file, char* fileName) {
	int i;
	if (file == 0) {

		for (i = 0; i < ncols * nrows; i++) {
			if (i % (ncols) == 0)
				printf("\n");
			printf("%lf ", cuCabs(M[i]) * cuCabs(M[i])); //cuCreal(M[i]));
		}
		printf("\n");

		for (i = 0; i < ncols * nrows; i++) {
			if (i % (ncols) == 0)
				printf("\n");
			printf("%lf + %lf *i ", cuCreal(M[i]), cuCimag(M[i]));
		}
	} else {
		FILE* pFile;
		pFile = fopen(fileName, "a");

		fprintf(pFile, "\n[");
		for (i = 0; i < ncols * nrows; i++) {
			if (i % (ncols) == 0 && i != 0)
				fprintf(pFile, "\n");
			fprintf(pFile, "%.4lf ", cuCabs(M[i]) * cuCabs(M[i]));
		}
		fprintf(pFile, "]\n");
		fprintf(pFile, "\n[");
		for (i = 0; i < ncols * nrows; i++) {
			if (i % (ncols) == 0 && i != 0)
				fprintf(pFile, "\n");
			fprintf(pFile, "%+.4lf %+.4lf *i ", cuCreal(M[i]), cuCimag(M[i]));
			//fprintf(pFile, "%4.0lf  ", cuCreal(M[i]));
		}
		fprintf(pFile, "]\n");

		fclose(pFile);
	}
}

void printmSquaresOnly(cuDoubleComplex *M, int nrows, int ncols, int file, char* fileName) {
	int i;
	if (file == 0) {

		for (i = 0; i < ncols * nrows; i++) {
			if (i % (ncols) == 0)
				printf("\n");
			printf("%lf ", cuCabs(M[i]) * cuCabs(M[i])); //cuCreal(M[i]));
		}
		printf("\n");

		for (i = 0; i < ncols * nrows; i++) {
			if (i % (ncols) == 0)
				printf("\n");
			printf("%lf + %lf *i ", cuCreal(M[i]), cuCimag(M[i]));
		}
	} else {
		FILE* pFile;
		pFile = fopen(fileName, "a");

		fprintf(pFile, "\n[");
		for (i = 0; i < ncols * nrows; i++) {
			if (i % (ncols) == 0 && i != 0)
				fprintf(pFile, "\n");
			fprintf(pFile, "%.4lf ", cuCabs(M[i]) * cuCabs(M[i]));
		}
		fprintf(pFile, "]\n");
		fclose(pFile);
	}
}
void printmComplexOnly(cuDoubleComplex *M, int nrows, int ncols, int file, char* fileName) {

	//The function output is matched to Matlab
	int i;
	if (file == 0) {

		for (i = 0; i < ncols * nrows; i++) {
			if (i % (ncols) == 0)
				printf("\n");
			printf("%lf ", cuCabs(M[i]) * cuCabs(M[i])); //cuCreal(M[i]));
		}
		printf("\n");

		for (i = 0; i < ncols * nrows; i++) {
			if (i % (ncols) == 0)
				printf("\n");
			printf("%lf + %lf *i ", cuCreal(M[i]), cuCimag(M[i]));
		}
	} else {
		FILE* pFile;
		pFile = fopen(fileName, "a");

		fprintf(pFile, "\n");
		for (i = 0; i < ncols * nrows; i++) {
			if (i % (ncols) == 0 && i != 0)
				fprintf(pFile, "\n");
			fprintf(pFile, "%.4lf %.4lf ", cuCreal(M[i]), cuCimag(M[i]));
			//fprintf(pFile, "%4.0lf  ", cuCreal(M[i]));
		}
		fprintf(pFile, "\n");
		fclose(pFile);
	}
}
void printim(int *M, int nrows, int ncols, int file, char* filename) {
	int i;
	if (file == 0) {
		for (i = 0; i < ncols * nrows; i++) {
			if (i % (ncols) == 0)
				printf("\n");
			printf("%i ", M[i]);
		}
		printf("\n");
	} else {
		FILE * pFile;
		pFile = fopen(filename, "a");
		for (i = 0; i < ncols * nrows; i++) {
			if (i % (ncols) == 0)
				fprintf(pFile, "\n");
			fprintf(pFile, "%i ", M[i]);
		}

		//	fprintf(pFile, "\n");
		fclose(pFile);
	}
}

void printfm(float *M, int nRows, int nCols, int file, char* fileName) {
	int i;
	if (file == 0) {
		for (i = 0; i < nCols * nRows; i++) {
			if (i % (nCols) == 0)
				printf("\n");
			printf("%.12f ", M[i]);
		}
		printf("\n");
	} else {
		FILE * pFile;
		pFile = fopen(fileName, "a");
		for (i = 0; i < nCols * nRows; i++) {
			if (i % (nCols) == 0)
				fprintf(pFile, "\n");
			fprintf(pFile, "%.12f ", M[i]);
		}

		fprintf(pFile, "\n");
		fclose(pFile);
	}
}
void fillWithZeros(cuDoubleComplex *Q, int rowsize, int colsize) {
	int i;
	for (i = 0; i < colsize * rowsize; i++) {
		Q[i] = make_cuDoubleComplex(0, 0);
	}
}
char* getTimeStamp(char* fileName) {
	char *buffer;
	buffer = (char*) calloc(26, sizeof(char));
	time_t timer;
	struct tm* tm_info;
	time(&timer);
	tm_info = localtime(&timer);

	strftime(buffer, 26, "%Y_%m_%d:%H:%M:%S", tm_info);
	// puts(buffer);

	return buffer;
}

//GPU UNITARY
void checkUnitary(cuDoubleComplex* Data) {
	int size = 3;
	B = new cuDoubleComplex[9];
	T = new cuDoubleComplex[9];
	E = new cuDoubleComplex[9];
	for (i = 0; i < sizeOfPopulation; i++) {
		printf("\nGPU");
		fillWithZeros(B, size, size);
		memcpy(B, Data + size * size * i, size * size * sizeof(cuDoubleComplex));
		memcpy(T, B, size * size * sizeof(cuDoubleComplex));
		complexConj(B, size, size);
		transpose(B, size, size);
		fillWithZeros(E, size, size);
		multiplyC(E, B, size, size, T, size, size);
		printm(E, size, size, 0, NULL);
	}
}

void prepareExpandedInteractionTemplates(cuDoubleComplex* result, cuDoubleComplex* resultD) {
	printf("Number of    rotation gates is: %i\n", numberOfRotationGates);
	printf("Number of interaction gates is: %i\n", interactionTemplatesNumber);
	cuDoubleComplex *interactionTemplate;
	int size = pow(2, numberOfWires);

	/*Block to fill interaction template*/
	interactionTemplate = new cuDoubleComplex[4 * 4];
	for (int i = 0; i < 16; i++) {
		interactionTemplate[i] = make_cuDoubleComplex(0, 0);
	}
	interactionTemplate[0] = make_cuDoubleComplex(0, -1);
	interactionTemplate[5] = make_cuDoubleComplex(0, 1);
	interactionTemplate[10] = make_cuDoubleComplex(0, 1);
	interactionTemplate[15] = make_cuDoubleComplex(0, -1);
	//	-- End of the block --
	int swapSize = 4;

	cuDoubleComplex* swapGate = new cuDoubleComplex[4 * 4];
	cuDoubleComplex* res = new cuDoubleComplex[size * size];
	cuDoubleComplex* oldRes = new cuDoubleComplex[size * size];
	eye(swapGate, 4);
	swapGate[5] = make_cuDoubleComplex(0, 0);
	swapGate[6] = make_cuDoubleComplex(1, 0);
	swapGate[9] = make_cuDoubleComplex(1, 0);
	swapGate[10] = make_cuDoubleComplex(0, 0);
	int matricesCount = 0;
	cuDoubleComplex* temp = new cuDoubleComplex[size * size];
	int i, j, k;
	for (i = 1; i < numberOfWires; i++) {
		//	printf("Starting generating interaction matrix\n");
		for (j = i + 1; j <= numberOfWires; j++) {
			//	printf("\t i=%i j=%i\n", i, j);
			eye(res, size);
			eye(oldRes, size);
			for (k = i + 1; k < j; k++) {
				//	printf("\t\t Multiplying by swaps forward \n");
				//	printf("\t\t i=%i j=%i k=%i\n", i, j, k);
				expandWithKron(NULL, temp, swapGate, swapSize, swapSize, k, k + 1, numberOfWires);
				multiplyC(res, oldRes, size, size, temp, size, size);
				copyArray(oldRes, res, size, size);
			}
			//printf("\tMultiplying by interaction template \n");
			expandWithKron(NULL, temp, interactionTemplate, swapSize, swapSize, i, i + 1, numberOfWires);

			multiplyC(res, oldRes, size, size, temp, size, size);
			copyArray(oldRes, res, size, size);

			for (k = j; k > i + 1; k--) {
				//printf("\t\t Multiplying by swaps backward \n");
				//printf("\t\t i=%i j=%i k=%i\n", i, j, k);
				expandWithKron(NULL, temp, swapGate, swapSize, swapSize, k - 1, k, numberOfWires);
				multiplyC(res, oldRes, size, size, temp, size, size);
				copyArray(oldRes, res, size, size);
			}
			copyArray(result + matricesCount * size * size, res, size, size);
			matricesCount++;
			//printf("Interaction matrix #%i generated \n\n", matricesCount);
		}
	}

	delete[] temp;
	CUDA_CHECK_RETURN(
			cudaMemcpy(resultD, result, size*size*interactionTemplatesNumber * sizeof(cuDoubleComplex), cudaMemcpyHostToDevice));
}

void allocateMemory(int size, int shift) {
	int matrixSize = size * size;
	segmentAlreadyUsedD = allocateIntArrayDevice(&segmentAlreadyUsedD, numberOfSegments);
	segmentAlreadyUsed = allocateIntArray(&segmentAlreadyUsed, numberOfSegments);
	axis = allocateIntArrayDevice(&axis, numberOfRotationGates);
	axis_h = allocateIntArray(&axis_h, numberOfRotationGates);

	circuits = allocateIntArrayDevice(&circuits, sizeOfIndividual * sizeOfPopulation);
	circuits_h = allocateIntArray(&circuits_h, sizeOfIndividual * sizeOfPopulation);

	//TODO what is the size of DATA???
	Data = allocateDoubleComplexArrayDevice(&Data, numberOfSegments * matrixSize);
	Data2 = allocateDoubleComplexArrayDevice(&Data2, numberOfSegments * matrixSize);
	Data3 = allocateDoubleComplexArrayDevice(&Data3, numberOfSegments * matrixSize);
	Data_h = allocateDoubleComplexArray(&Data_h, numberOfSegments * matrixSize);
	tempfitness = allocateDoubleComplexArrayDevice(&tempfitness, size*size*sizeOfPopulation);


	final_circuit = allocateDoubleComplexArrayDevice(&final_circuit, sizeOfPopulation * matrixSize);
	final_circuit_h = allocateDoubleComplexArray(&final_circuit_h, sizeOfPopulation * matrixSize);
	temp_circuit = allocateDoubleComplexArrayDevice(&temp_circuit, sizeOfPopulation * matrixSize);

	fitness = allocateFloatArrayDevice(&fitness, sizeOfPopulation);

	fitness_h = allocateFloatArray(&fitness_h, sizeOfPopulation);

	gateMatrices = allocateDoubleComplexArrayDevice(&gateMatrices, numberOfRotationGates * 4);
	gateMatrices_h = allocateDoubleComplexArray(&gateMatrices_h, numberOfRotationGates * 4);

	//TODO is it still in use?
	identitym = allocateDoubleComplexArray(&identitym, numberOfRotationGates * 4);
	identitymD = allocateDoubleComplexArrayDevice(&identitymD, numberOfRotationGates * 4);

	interactionMatricesMemory = allocateDoubleComplexArray(&interactionMatricesMemory,
			interactionTemplatesNumber * matrixSize);
	interactionMatricesMemoryD = allocateDoubleComplexArrayDevice(&interactionMatricesMemoryD,
			interactionTemplatesNumber * matrixSize);
	MatricesForQutritsEvolution = allocateDoubleComplexArrayDevice(&MatricesForQutritsEvolution,
			9 * numberOfRotationGates);

	//TODO is it still used?
	parents = allocateIntArrayDevice(&parents, numberOfSegments);
	parents_h = allocateIntArray(&parents_h, numberOfSegments);

	Qubits = allocateDoubleComplexArrayDevice(&Qubits, numberOfSegments);
	oldQubits = allocateDoubleComplexArrayDevice(&oldQubits, numberOfSegments);

	Qubits_h = allocateDoubleComplexArray(&Qubits_h, numberOfSegments);
	Qubitst = allocateDoubleComplexArrayDevice(&Qubitst, numberOfSegments);

	Qutrits = allocateDoubleComplexArrayDevice(&Qutrits, 3 * numberOfRotationGates);
	oldQutrits = allocateDoubleComplexArrayDevice(&oldQutrits, 3 * numberOfRotationGates);

	Qutrits_h = allocateDoubleComplexArray(&Qutrits_h, 3 * numberOfRotationGates);
	Qutritst = allocateDoubleComplexArrayDevice(&Qutritst, 3 * numberOfRotationGates);

	segmentfitness = allocateFloatArrayDevice(&segmentfitness, numberOfSegments);
	oldSegmentFitness = allocateFloatArrayDevice(&oldSegmentFitness, numberOfSegments);
	segmentfitness_h = allocateFloatArray(&segmentfitness_h, numberOfSegments);

	//TODO still in use?
	segmentHit = allocateIntArray(&segmentHit, sizeOfIndividual * numberOfSegments);
	segmentHit_d = allocateIntArrayDevice(&segmentHit_d, sizeOfIndividual * numberOfSegments);

	//TODO is it still used??
	segmPosfitness = allocateFloatArray(&segmPosfitness, sizeOfIndividual * numberOfSegments);
	segmPosfitness_d = allocateFloatArrayDevice(&segmPosfitness_d, sizeOfIndividual * numberOfSegments);

	solut = allocateIntArray(&solut, 1);
	solut_d = allocateIntArrayDevice(&solut_d, 1);
	bsolut = allocateFloatArray(&bsolut, 1);
	bsolutI = allocateIntArray(&bsolutI, 1);
	bsolutID = allocateIntArrayDevice(&bsolutID, 1);
	bsolutD = allocateFloatArrayDevice(&bsolutD, 1);
	sumCols = allocateFloatArray(&sumCols, sizeOfIndividual);
	sumColsD = allocateFloatArrayDevice(&sumColsD, sizeOfIndividual);
	sumOfFitness = allocateFloatArrayDevice(&sumOfFitness, 1);
	sumOfFitness_h = allocateFloatArray(&sumOfFitness_h, 1);
	//TODO is it still used?
	sumRowsD = allocateFloatArrayDevice(&sumRowsD, numberOfSegments);
	sumRows = allocateFloatArray(&sumRows, numberOfSegments);
	sumSegmentCols = allocateFloatArray(&sumSegmentCols, numberOfSegments);
	sumSegmentRows = allocateFloatArray(&sumSegmentRows, 1);
	sumSegmentColsD = allocateFloatArrayDevice(&sumSegmentColsD, numberOfSegments);
	sumSegmentRowsD = allocateFloatArrayDevice(&sumSegmentRowsD, 1);

	target = allocateDoubleComplexArrayDevice(&target, matrixSize);
	target_h = allocateDoubleComplexArray(&target_h, matrixSize);

	for (int i = 0; i < 3 * numberOfRotationGates; i++) {
		Qutrits_h[i] = make_cuDoubleComplex(0, 0);
	}
	solut[0] = -1;
	bsolut[0] = 0.0;
	bsolutI[0] = 0;
	cudaMalloc((void **) &QutritOpers, 3 * numberOfRotationGates * sizeof(QutritOper));
	cudaMalloc((void **) &devStates, numberOfSegments * sizeof(curandState));
}

void setTarget(char* fileName, int size) {
	parseFile(fileName, target_h);
	printf("\n\nDebug of Target");
	sprintf(fileName, "%s/%s.txt", "././Matlab/InputFiles", "matlabRecentTarget");
	FILE * pFile = fopen(fileName, "w");
	if (pFile == NULL) {
		printf("Cannot open output file for Matlab, at %s, please fix", fileName);
		printm(target_h, size, size, 0, NULL);
		return;
	} else {
		int i;
		int size = 1 << numberOfWires;
		for (i = 0; i < size * size; i++) {
			if (i % size == 0 && i != 0)
				fprintf(pFile, "\n");
			fprintf(pFile, "%i ", int(cuCreal(*(target_h + i))));
		}
		fclose(pFile);
	}

}

void checkAndSetGPU() {
	int nDevices;

	cudaGetDeviceCount(&nDevices);
	for (int i = 0; i < nDevices; i++) {
		cudaDeviceProp prop;
		cudaGetDeviceProperties(&prop, i);
		printf("Device Number: %d\n", i);
		printf("  Device name: %s\n", prop.name);
		printf("  Memory Clock Rate (KHz): %d\n", prop.memoryClockRate);
		printf("  Memory Bus Width (bits): %d\n", prop.memoryBusWidth);
		printf("  Peak Memory Bandwidth (GB/s): %f\n\n",
				2.0 * prop.memoryClockRate * (prop.memoryBusWidth / 8) / 1.0e6);
	}
	cudaSetDevice(0);
}
void debugSegmentPositionFitness(int type) {

	if (type == 1) {
		cudaMemcpy(segmPosfitness, segmPosfitness_d, numberOfSegments * sizeOfIndividual * sizeof(float),
				cudaMemcpyDeviceToHost);
		cudaDeviceSynchronize();
		printfm(segmPosfitness, numberOfSegments, sizeOfIndividual, 0, NULL);
		printf("\nDebug of sumRows after calculating it\n");
		printfm(sumRows, numberOfSegments, 1, 0, NULL);
		printf("\nDebug of sumCols after calculating it\n");
		printfm(sumCols, sizeOfIndividual, 1, 0, NULL);
	} else if (type == 2) {
		cudaMemcpy(segmPosfitness, segmPosfitness_d, numberOfSegments * sizeOfIndividual * sizeof(float),
				cudaMemcpyDeviceToHost);
		cudaDeviceSynchronize();
		sprintf(fileName, "%s/%s.txt", dirName, "SegmentPositionFitness");
		FILE * pFile;
		pFile = fopen(fileName, "w");
		fclose(pFile);
		printfm(segmPosfitness, numberOfSegments, sizeOfIndividual, 1, fileName);
		pFile = fopen(fileName, "a");
		fprintf(pFile, "\nDebug of sumRows after calculating it\n");
		fclose(pFile);
		printfm(sumRows, numberOfSegments, 1, 1, fileName);
		pFile = fopen(fileName, "a");
		fprintf(pFile, "\nDebug of sumCols after calculating it\n");
		fclose(pFile);
		printfm(sumCols, sizeOfIndividual, 1, 1, fileName);
	}
}
void debugGeneratedCircuits(int type) {
	if (type == 1) {
		int size = 1 << numberOfWires;
		cudaMemcpy(circuits_h, circuits, sizeOfIndividual * sizeOfPopulation * sizeof(int), cudaMemcpyDeviceToHost);
		cudaMemcpy(Data_h, Data2, numberOfSegments * size * size * sizeof(cuDoubleComplex), cudaMemcpyDeviceToHost);

		for (int j = 0; j < sizeOfPopulation; j++) {

			printf("\nCircuit #%i \n", j);
			for (int q = 0; q < sizeOfIndividual; q++)
				printf("%i ", circuits_h[j * sizeOfIndividual + q]);
			/*	printf("\n");
			 for (int q = 0; q < sizeOfIndividual; q++)
			 printm(Data_h + circuits_h[j * sizeOfIndividual + q] * size * size, size, size, 0, NULL);
			 */
		}

	}
	if (type == 2) {
		int size = 1 << numberOfWires;
		cudaMemcpy(circuits_h, circuits, sizeOfIndividual * sizeOfPopulation * sizeof(int), cudaMemcpyDeviceToHost);
		cudaMemcpy(Data_h, Data2, numberOfSegments * size * size * sizeof(cuDoubleComplex), cudaMemcpyDeviceToHost);

		sprintf(fileName, "%s/%s.txt", dirName, "Circuits");
		FILE * pFile;
		pFile = fopen(fileName, "w");
		for (int j = 0; j < sizeOfPopulation; j++) {

			pFile = fopen(fileName, "a");
			fprintf(pFile, "\nCircuit #%i \n", j);
			for (int q = 0; q < sizeOfIndividual; q++)
				fprintf(pFile, "%i ", circuits_h[j * sizeOfIndividual + q]);
			fprintf(pFile, "\n");
			fclose(pFile);
			for (int q = 0; q < sizeOfIndividual; q++)
				printm(Data_h + circuits_h[j * sizeOfIndividual + q] * size * size, size, size, 1, fileName);
		}

	}

}
void debugAxis(int type) {
	if (type == 1) {
		cudaMemcpy(axis_h, axis, numberOfRotationGates * sizeof(int), cudaMemcpyDeviceToHost);
		printim(axis_h, 1, numberOfRotationGates, 0, NULL);
	}
	if (type == 2) {
		cudaMemcpy(axis_h, axis, numberOfRotationGates * sizeof(int), cudaMemcpyDeviceToHost);
		sprintf(fileName, "%s/%s.txt", dirName, "Axis");
		FILE * pFile;
		pFile = fopen(fileName, "a");
		//fprintf(pFile, "Qutrit Evolution is ON numberOfMeasurements is %i MutationRange is:%.4f",
		//		numberOfMeasurements, mutationRange);
		fclose(pFile);
		printim(axis_h, 1, numberOfRotationGates, 1, fileName);
	}

}
void debugFinalCircuitMatrices(int type) {
	if (type == 1) {
		int size = 1 << numberOfWires;
		cudaMemcpy(final_circuit_h, final_circuit, size * size * sizeOfPopulation * sizeof(cuDoubleComplex),
				cudaMemcpyDeviceToHost);

		for (int j = 0; j < sizeOfPopulation; j++) {

			printf("\nCircuit #%i \n", j);
			printm(final_circuit_h + j * size * size, size, size, 0, NULL);
		}

	}
	if (type == 2) {
		int size = 1 << numberOfWires;
		cudaMemcpy(final_circuit_h, final_circuit, size * size * sizeOfPopulation * sizeof(cuDoubleComplex),
				cudaMemcpyDeviceToHost);

		sprintf(fileName, "%s/%s.txt", dirName, "FinalCircuitMatrices");
		FILE * pFile;
		pFile = fopen(fileName, "w");
		for (int j = 0; j < sizeOfPopulation; j++) {

			pFile = fopen(fileName, "a");
			fprintf(pFile, "\nCircuit #%i \n", j);
			fclose(pFile);
			printm(final_circuit_h + j * size * size, size, size, 1, fileName);
		}

		/*	printfm(segmPosfitness, generatedGates * numberOfWires + interactionTemplatesNumber, sizeOfIndividual, 1, fileName);
		 pFile = fopen(fileName, "a");
		 fprintf(pFile, "\nDebug of sumRows after calculating it\n");
		 fclose(pFile);
		 printfm(sumRows, generatedGates * numberOfWires + interactionTemplatesNumber, 1, 1, fileName);
		 pFile = fopen(fileName, "a");
		 fprintf(pFile, "\nDebug of sumCols after calculating it\n");
		 fclose(pFile);
		 printfm(sumCols, sizeOfIndividual, 1, 1, fileName);*/
	}
}
void generateSwap(int size) {
	//creation and expansion SWAP gates
	if (numberOfWires > 2) {
		constructSwapGates<<<blocksPerGrid,threadsPerBlock>>>(Data+sizeOfPopulation*size*size,size);
		CUDA_CHECK_RETURN(cudaMemcpy(Data_h,Data+numberOfRotationGates*size*size,(numberOfWires-1)*size*size*sizeof(cuDoubleComplex),cudaMemcpyDeviceToHost ));
		printf ("\n\n Debug of swap");
		for (int i = 0; i < numberOfWires-1;i++) {
			printm(Data_h+i*size*size,4,4,0,NULL);
		}

		printf ("\n\n Debug of identities");

		for (int i = 0; i < numberOfWires-1;i++) {
			int temps = (int) pow(2,i);
			eyeD<<<blocksPerGrid,threadsPerBlock>>>(identitymD,temps,1);
			//
			//printm(identitym,temps,temps,0,NULL);
			int offset = sizeOfPopulation*size*size+i*size*size;
			kronGPU<<<blocksPerGrid,threadsPerBlock>>>(Data3+offset, identitymD, temps, temps, Data+offset, 4,4, 1);
			int oldsize = temps * 4;
			temps = (int) pow(2,numberOfWires-i-2);
			eyeD<<<blocksPerGrid,threadsPerBlock>>>(identitymD,temps,1);
			kronGPU<<<blocksPerGrid,threadsPerBlock>>>(Data+offset,Data3+offset, oldsize,oldsize,identitymD, temps, temps, 1);
		}

		CUDA_CHECK_RETURN(cudaMemcpy(Data2+numberOfRotationGates*size*size,Data+sizeOfPopulation*size*size,(numberOfWires-1)*size*size*sizeof(cuDoubleComplex),cudaMemcpyDeviceToDevice ));
		CUDA_CHECK_RETURN(cudaMemcpy(Data3+numberOfRotationGates*size*size,Data+sizeOfPopulation*size*size,(numberOfWires-1)*size*size*sizeof(cuDoubleComplex),cudaMemcpyDeviceToDevice ));
		CUDA_CHECK_RETURN(cudaMemcpy(Data_h,Data+numberOfRotationGates*size*size,(numberOfWires-1)*size*size*sizeof(cuDoubleComplex),cudaMemcpyDeviceToHost ));
		printf ("\n\n Debug of swap");
		for (int i = 0; i < numberOfWires-1;i++) {
			printm(Data_h+i*size*size,size,size,0,NULL);
		}

	}

	eyeD<<<blocksPerGrid,threadsPerBlock>>>(identitymD,size,1);
	CUDA_CHECK_RETURN(
			cudaMemcpy(Data2+(sizeOfPopulation+numberOfWires-1)*size*size,identitymD,size*size*sizeof(cuDoubleComplex),cudaMemcpyDeviceToDevice ));
	CUDA_CHECK_RETURN(
			cudaMemcpy(Data3+(sizeOfPopulation+numberOfWires-1)*size*size,identitymD,size*size*sizeof(cuDoubleComplex),cudaMemcpyDeviceToDevice ));
	CUDA_CHECK_RETURN(
			cudaMemcpy(Data +(sizeOfPopulation+numberOfWires-1)*size*size,identitymD,size*size*sizeof(cuDoubleComplex),cudaMemcpyDeviceToDevice ));
	CUDA_CHECK_RETURN(cudaDeviceSynchronize());
	CUDA_CHECK_RETURN(
			cudaMemcpy(Data_h,Data+(sizeOfPopulation+numberOfWires-1)*size*size, size*size*sizeof(cuDoubleComplex),cudaMemcpyDeviceToHost));

	printf ("\n\n Debug of global identities");
	printm(Data_h,size,size,0,NULL);

	// End of generation of Swap gates - unnecessary

}
void debugQubits(int type) {
	if (type == 1) {
		CUDA_CHECK_RETURN(
				cudaMemcpy(Qubits_h,Qubits,(numberOfSegments)*sizeof(cuDoubleComplex),cudaMemcpyDeviceToHost ));
		printm(Qubits_h, 1, numberOfSegments, 0, NULL);
	}
	if (type == 2) {

		sprintf(fileName, "%s/%s.txt", dirName, "Qubits");

		CUDA_CHECK_RETURN(
				cudaMemcpy(Qubits_h,Qubits,(numberOfSegments)*sizeof(cuDoubleComplex),cudaMemcpyDeviceToHost ));
		printm(Qubits_h, 1, (numberOfRotationGates + interactionTemplatesNumber), 1,
		fileName);
	}
}
void debugQutrits(int type) {
	if (type == 2) {
		FILE* pFile;

		sprintf(fileName, "%s/%s.txt", dirName, "Qutrits");
		cudaMemcpy(Qutrits_h, Qutrits, 3 * numberOfRotationGates * sizeof(cuDoubleComplex), cudaMemcpyDeviceToHost);
		for (int j = 0; j < numberOfRotationGates; j++) {
			pFile = fopen(fileName, "a");
			fprintf(pFile, "Qutrit %i\n", j);
			fclose(pFile);
			printm(Qutrits_h + j * 1 * 3, 3, 1, 1, fileName);
		}
	}
}

void debugPopulation(int type) {
	//TODO fix on whcih wire the matrix is applied
	if (type == 1) {
		printf("\nPlease reimplement\n");
	}
	if (type == 2) {
		cudaMemcpy(gateMatrices_h, gateMatrices, numberOfRotationGates * 4 * sizeof(cuDoubleComplex),
				cudaMemcpyDeviceToHost);

		int size = 1 << numberOfWires;
		cudaMemcpy(Qubits_h, Qubits, numberOfSegments * sizeof(cuDoubleComplex), cudaMemcpyDeviceToHost);
		cudaMemcpy(Data_h, Data2, numberOfSegments * size * size * sizeof(cuDoubleComplex), cudaMemcpyDeviceToHost);
		sprintf(fileName, "%s/%s.txt", dirName, "FinalPopulation");
		FILE * pFile;
		pFile = fopen(fileName, "w");
		fprintf(pFile, "Number of wires is  %i\n", numberOfWires);
		fclose(pFile);
		for (int j = 0; j < numberOfRotationGates; j++) {
			pFile = fopen(fileName, "a");
			fprintf(pFile, "\n\nMatrix template number %i", j + 1);
			fclose(pFile);
			printm(gateMatrices_h + 4 * j, 2, 2, 1, fileName);
			pFile = fopen(fileName, "a");
			fprintf(pFile, "\nMatrix number %i, on %i wire", j + 1, j / sizeOfPopulation + 1);
			fclose(pFile);
			printm(Data_h + size * size * j, size, size, 1, fileName);
		}
		for (int j = 0; j < interactionTemplatesNumber; j++) {
			pFile = fopen(fileName, "a");
			fprintf(pFile, "\n\nInteraction Matrix template number %i", j);
			fprintf(pFile, "\nParameter is %.8f + i * %.8f", cuCreal(Qubits_h[j + numberOfRotationGates]),
					cuCimag(Qubits_h[j + numberOfRotationGates]));
			fclose(pFile);
			pFile = fopen(fileName, "a");
			fprintf(pFile, "\nMatrix number %i, on %i wire", numberOfRotationGates + j + 1, j / sizeOfPopulation + 1);
			fclose(pFile);
			printm(Data_h + (numberOfRotationGates + j) * size * size, size, size, 1, fileName);
		}
	}
}
void printResults() {
	int size = 1 << numberOfWires;
	sprintf(dirName, "%s%s", "./results/", timeStamp);
	if (stat(dirName, &st) == -1) {
		mkdir(dirName, 0700);
	}
	debugGeneratedCircuits(2);
	debugPopulation(2);
	FILE * pFile;
	cudaMemcpy(fitness_h, fitness, getSizeOfFloatArrayDevice(fitness) * sizeof(float), cudaMemcpyDeviceToHost);
	sprintf(fileName, "%s/%s.txt", dirName, "FinalResults");
	pFile = fopen(fileName, "a");
	fprintf(pFile, "Debug of Results: number of wires is %i index is %i fitness is %f \nTarget is:", numberOfWires,
			(*solut), fitness_h[(*solut)]);
	fclose(pFile);
	printm(target_h, size, size, 1, fileName);
	cudaMemcpy(final_circuit_h, final_circuit, size * size * (sizeOfPopulation) * sizeof(cuDoubleComplex),
			cudaMemcpyDeviceToHost);

	printm(final_circuit_h + (*solut) * size * size, size, size, 1, fileName);
	pFile = fopen(fileName, "a");

	cudaMemcpy(Qubits_h, Qubits, getSizeOfDoubleComplexArrayDevice(Qubits) * sizeof(cuDoubleComplex),
			cudaMemcpyDeviceToHost);
	cudaMemcpy(Qutrits_h, Qutrits, getSizeOfDoubleComplexArrayDevice(Qutrits) * sizeof(cuDoubleComplex),
			cudaMemcpyDeviceToHost);
	cudaMemcpy(circuits_h, circuits, sizeOfPopulation * sizeOfIndividual * sizeof(int), cudaMemcpyDeviceToHost);
	cudaMemcpy(axis_h, axis, numberOfRotationGates * sizeof(int), cudaMemcpyDeviceToHost);
	fprintf(pFile, "\nThe search was completed by %s\nQuantum encoded:\n", getTimeStamp(NULL));
	for (i = 0; i < sizeOfIndividual; i++) {
		int ind = circuits_h[(*solut) * sizeOfIndividual + i];
		fprintf(pFile, "Index of the circuit is: %i\n", ind);
		if (ind >= numberOfRotationGates) {
			//Shifted index denotes the start of Interaction gates
			int shiftedInd = ind - numberOfRotationGates;
			int relInd = shiftedInd / (sizeOfPopulation * sizeOfIndividual);
			fprintf(pFile, "Interaction between %i and %i with parameter %lf+i*%lf\n", (relInd % (numberOfWires) + 1),
					((relInd % (numberOfWires) + 2) % (numberOfWires + 1)), Qubits_h[ind].x, Qubits_h[ind].y);
		} else {
			fprintf(pFile, "Qubit %i : [%lf + i*%lf]  Qutrit %i [%lf + i * %lf,%lf + i * %lf, %lf + i * %lf] \n", i,
					Qubits_h[ind].x, Qubits_h[ind].y, i, Qutrits_h[ind * 3].x, Qutrits_h[3 * ind].y,
					Qutrits_h[ind * 3 + 1].x, Qutrits_h[3 * ind + 1].y, Qutrits_h[ind * 3 + 2].x,
					Qutrits_h[3 * ind + 2].y);
		}
	}

	fclose(pFile);

	sprintf(fileName, "%s/%s.txt", dirName, "matlabExportFinal");
	pFile = fopen(fileName, "w");
	fprintf(pFile, "%i %i", numberOfWires, sizeOfIndividual);
	for (i = 0; i < sizeOfIndividual; i++) {
		int ind = circuits_h[(*solut) * sizeOfIndividual + sizeOfIndividual - i - 1];
		int index1, index2, direction;
		float real, imaginary;
		if (ind >= numberOfRotationGates) {
			int shiftedInd = ind - numberOfRotationGates;
			int relInd = shiftedInd / (sizeOfPopulation * sizeOfIndividual);
			index1 = (relInd % (numberOfWires) + 1);
			index2 = (index1 + 1);
			real = Qubits_h[ind].x;
			imaginary = Qubits_h[ind].y;
			direction = 2;
		} else {
			index1 = ind / sizeOfPopulation / sizeOfIndividual + 1;
			index2 = ind / sizeOfPopulation / sizeOfIndividual + 1;
			real = Qubits_h[ind].x;
			imaginary = Qubits_h[ind].y;
			direction = axis_h[ind];
		}

		fprintf(pFile, "\n%i %i %i %lf %lf", index1, index2, direction, real, imaginary);
	}
	fclose(pFile);

}
void printInterimResults() {
	int size = 1 << numberOfWires;
	sprintf(dirName, "%s%s", "./results/", timeStamp);
	if (stat(dirName, &st) == -1) {
		mkdir(dirName, 0700);
	}
	//debugGeneratedCircuits(2);
	//debugPopulation(2);
	FILE * pFile;
	cudaMemcpy(fitness_h, fitness, getSizeOfFloatArrayDevice(fitness) * sizeof(float), cudaMemcpyDeviceToHost);
	sprintf(fileName, "%s/%s.txt", dirName, "BestResults");
	sprintf(sqFileName, "%s/%s.txt", dirName, "BestResults.sq");
	sprintf(complexFileName, "%s/%s.txt", "././Matlab/InputFiles", "matlabRecentMatrices");

	pFile = fopen(sqFileName, "w");
	fclose(pFile);

	pFile = fopen(complexFileName, "w");
	fclose(pFile);
	pFile = fopen(fileName, "w");
	fprintf(pFile, "Debug of Results: number of wires is %i index is %i fitness is %f \nTarget is:", numberOfWires,
			(*bsolutI), fitness_h[(*bsolutI)]);
	fclose(pFile);
	printm(target_h, size, size, 1, fileName);
	printParameters(1, fileName);
	pFile = fopen(fileName, "a");
	fprintf(pFile, "Solution is:");
	fclose(pFile);

	cudaMemcpy(final_circuit_h, final_circuit, size * size * (sizeOfPopulation) * sizeof(cuDoubleComplex),
			cudaMemcpyDeviceToHost);

	printm(final_circuit_h + (*bsolutI) * size * size, size, size, 1, fileName);
	pFile = fopen(fileName, "a");

	cudaMemcpy(Qubits_h, Qubits, getSizeOfDoubleComplexArrayDevice(Qubits) * sizeof(cuDoubleComplex),
			cudaMemcpyDeviceToHost);
	cudaMemcpy(Data_h, Data2, getSizeOfDoubleComplexArrayDevice(Data2) * sizeof(cuDoubleComplex),
			cudaMemcpyDeviceToHost);
	cudaMemcpy(Qutrits_h, Qutrits, getSizeOfDoubleComplexArrayDevice(Qutrits) * sizeof(cuDoubleComplex),
			cudaMemcpyDeviceToHost);
	cudaMemcpy(circuits_h, circuits, sizeOfPopulation * sizeOfIndividual * sizeof(int), cudaMemcpyDeviceToHost);
	cudaMemcpy(axis_h, axis, numberOfRotationGates * sizeof(int), cudaMemcpyDeviceToHost);
	fprintf(pFile, "\nThe search was completed by %s\nQuantum encoded:\n", getTimeStamp(NULL));
	for (i = 0; i < sizeOfIndividual; i++) {
		int ind = circuits_h[(*bsolutI) * sizeOfIndividual + i];
		fprintf(pFile, "Index of the circuit is: %i\n", ind);
		printm(Data_h + ind * size * size, size, size, 1, fileName);
		printmSquaresOnly(Data_h + ind * size * size, size, size, 1, sqFileName);
		printmComplexOnly(Data_h + ind * size * size, size, size, 1, complexFileName);

		if (ind >= numberOfRotationGates) {
			//Shifted index denotes the start of Interaction gates
			int shiftedInd = ind - numberOfRotationGates;
			int relInd = shiftedInd / (sizeOfPopulation * sizeOfIndividual);
			int index1, index2;
			int q, w, counter = 0;
			for (q = 1; q < numberOfWires; q++) {
				for (w = q + 1; w <= numberOfWires; w++) {
					if (counter == relInd) {
						//printf("%i %i %i\n", relInd, q, w);
						index1 = q;
						index2 = w;
						counter++;
						break;
					}
					counter++;
				}
			}
			fprintf(pFile, "Interaction between %i and %i with parameter %lf+i*%lf  relInd is %i\n", index1, index2,
					Qubits_h[ind].x, Qubits_h[ind].y, relInd);
		} else {
			fprintf(pFile, "Qubit %i : [%lf + i*%lf]  Qutrit %i [%lf + i * %lf,%lf + i * %lf, %lf + i * %lf] \n", i,
					Qubits_h[ind].x, Qubits_h[ind].y, i, Qutrits_h[ind * 3].x, Qutrits_h[3 * ind].y,
					Qutrits_h[ind * 3 + 1].x, Qutrits_h[3 * ind + 1].y, Qutrits_h[ind * 3 + 2].x,
					Qutrits_h[3 * ind + 2].y);
		}
	}
	fclose(pFile);
	//Indicate Matlab directory here...
	sprintf(fileName, "%s/%s.txt", "././Matlab/InputFiles", "matlabInterimExport");
	pFile = fopen(fileName, "w");
	if (pFile == NULL) {
		printf("Cannot open output file for Matlab, at %s, please fix", fileName);
		return;
	}
	fprintf(pFile, "%i %i", numberOfWires, sizeOfIndividual);
	for (i = 0; i < sizeOfIndividual; i++) {
		int ind = circuits_h[(*bsolutI) * sizeOfIndividual + i];
		int index1, index2, direction;
		float real, imaginary;
		if (ind >= numberOfRotationGates) {
			int shiftedInd = ind - numberOfRotationGates;
			int relInd = shiftedInd / (sizeOfPopulation * sizeOfIndividual);
			int q, w, counter = 0;
			for (q = 1; q < numberOfWires; q++) {
				for (w = q + 1; w <= numberOfWires; w++) {
					if (counter == relInd) {
						index1 = q;
						index2 = w;
						counter++;
						break;
					}
					counter++;
				}
			}
			real = Qubits_h[ind].x;
			imaginary = Qubits_h[ind].y;
			direction = 2;
		} else {
			index1 = ind / sizeOfPopulation / sizeOfIndividual + 1;
			index2 = ind / sizeOfPopulation / sizeOfIndividual + 1;
			real = Qubits_h[ind].x;
			imaginary = Qubits_h[ind].y;
			direction = axis_h[ind];
		}

		fprintf(pFile, "\n%i %i %i %lf %lf", index1, index2, direction, real, imaginary);
	}
	fclose(pFile);

}
void debugOfRearrangedTemplates(int type, int iteration) {
	if (iteration > 1)
		return;
	if (type == 1) {

		cudaMemcpy(Data_h, Data, 4 * numberOfWires * numberOfWires * sizeOfPopulation * sizeof(cuDoubleComplex),
				cudaMemcpyDeviceToHost);
		for (int j = 0; j < sizeOfPopulation * numberOfWires * numberOfWires; j++) {
			printf("\nMatrix number %i", j + 1);
			printm(Data_h + 2 * 2 * j, 2, 2, 0, NULL);
		}
	}
	if (type == 2) {
		int size = 1 << numberOfWires;
		cudaMemcpy(Data_h, Data, numberOfRotationGates * size * size * sizeof(cuDoubleComplex), cudaMemcpyDeviceToHost);
		sprintf(fileName, "%s/%s.txt", dirName, "RearrangedTemplates");
		FILE * pFile;
		pFile = fopen(fileName, "a");
		for (int j = 0; j < numberOfRotationGates * numberOfWires; j++) {
			pFile = fopen(fileName, "a");
			fprintf(pFile, "\nMatrix number %i", j + 1);
			fclose(pFile);
			printm(Data_h + 2 * 2 * j, 2, 2, 1, fileName);
		}
	}
}

void debugExpandedTemplates(int type, int iteration) {
	if (type == 1) {
		int size = 1 << numberOfWires;
		cudaMemcpy(gateMatrices_h, gateMatrices, numberOfRotationGates * 4 * sizeof(cuDoubleComplex),
				cudaMemcpyDeviceToHost);
		cudaMemcpy(Data_h, Data2, numberOfWires * sizeOfPopulation * size * size * sizeof(cuDoubleComplex),
				cudaMemcpyDeviceToHost);
		for (int j = 0; j < numberOfRotationGates; j++) {
			printf("\n\nMatrix template number %i", j);
			printm(gateMatrices_h + 4 * j, 2, 2, 0, NULL);
			printf("\nMatrix number %i, on %i wire", j, j / sizeOfPopulation / sizeOfIndividual + 1);
			printm(Data_h + size * size * j, size, size, 0, NULL);
		}
	}
	if (type == 2) {
		int size = 1 << numberOfWires;
		cudaMemcpy(gateMatrices_h, gateMatrices, numberOfRotationGates * 4 * sizeof(cuDoubleComplex),
				cudaMemcpyDeviceToHost);

		cudaMemcpy(Data_h, Data2, numberOfRotationGates * size * size * sizeof(cuDoubleComplex),
				cudaMemcpyDeviceToHost);
		sprintf(fileName, "%s/%s.txt", dirName, "ExpandedTemplates");
		FILE * pFile;
		pFile = fopen(fileName, "a");
		fprintf(pFile, "Number of wires is  %i\n", numberOfWires);
		fclose(pFile);
		for (int j = 0; j < numberOfRotationGates; j++) {
			pFile = fopen(fileName, "a");
			fprintf(pFile, "\n\nMatrix template number %i", j);
			fclose(pFile);
			printm(gateMatrices_h + 4 * j, 2, 2, 1, fileName);
			pFile = fopen(fileName, "a");
			fprintf(pFile, "\nMatrix number %i, on %i wire", j, j / sizeOfPopulation / sizeOfIndividual + 1);
			fclose(pFile);
			printm(Data_h + size * size * j, size, size, 1, fileName);
		}
	}
}




int main(int argc, char *argv[]) {
	/*if (interactionTemplatesNumber > numberOfRotationGates) {
	 printf("INTERACTION GATES COUNT SHOULD NOT BE BIGGER THAN GENERATEDGATES");
	 return 0;
	 }*/
	cublasHandle_t handle;
	cublasCreate(&handle);
	cudaStream_t* streams;
	//streams = createStreams(8);
	int shift = 0;
	if (numberOfWires > 2)
		shift = numberOfWires;
	int size = 1 << numberOfWires;

	allocateMemory(size, shift);

	setTarget(argv[1], size);
	checkAndSetGPU();
	sqFileName = (char*) calloc(123, sizeof(char));
	complexFileName = (char*) calloc(123, sizeof(char));
	fileName = (char*) calloc(123, sizeof(char));
	dirName = (char*) calloc(123, sizeof(char));
	timeStamp = getTimeStamp(argv[1]);
	sprintf(dirName, "%s", "./tests/");
	if (stat(dirName, &st) == -1) {
		mkdir(dirName, 0700);
	}
	sprintf(dirName, "%s", "./DebugOutput/");
	if (stat(dirName, &st) == -1) {
		mkdir(dirName, 0700);
	}
	sprintf(dirName, "%s", "./results/");
	if (stat(dirName, &st) == -1) {
		mkdir(dirName, 0700);
	}
	sprintf(dirName, "%s%s", "./DebugOutput/", timeStamp);
	if (stat(dirName, &st) == -1) {
		mkdir(dirName, 0700);
	}
	prepareExpandedInteractionTemplates(interactionMatricesMemory, interactionMatricesMemoryD);

	//Debug of generated interaction matrices
	/*	for (int i = 0; i < interactionTemplatesNumber; i++) {
	 printf("Interaction Matrices debug %i", i);
	 printm(interactionMatricesMemory + i * size * size, size, size, 0, NULL);
	 }
	 */
	float oldBSolut = 0.00;
	setup_kernel<<<blocksPerGrid,threadsPerBlock>>>(devStates, time(NULL));
	initQutrits<<<blocksPerGrid,threadsPerBlock>>>(Qutrits,devStates);

	CUDA_CHECK_RETURN(cudaMemcpy(solut_d, solut, 1 * sizeof(int), cudaMemcpyHostToDevice));
	CUDA_CHECK_RETURN(cudaMemcpy(bsolutD, bsolut, 1 * sizeof(float), cudaMemcpyHostToDevice));
	CUDA_CHECK_RETURN(cudaMemcpy(bsolutID, bsolutI, 1 * sizeof(int), cudaMemcpyHostToDevice));
	CUDA_CHECK_RETURN(cudaMemcpy(target, target_h, size * size * sizeof(cuDoubleComplex), cudaMemcpyHostToDevice));
	printf("hello");
	initQutritOpers<<<blocksPerGrid,threadsPerBlock>>>(QutritOpers,devStates);
	constructMatricesForQutritEvolution<<<blocksPerGrid,threadsPerBlock>>>(QutritOpers,MatricesForQutritsEvolution);
	initQubits<<<blocksPerGrid,threadsPerBlock>>>(Qubits,devStates);
	cudaDeviceSynchronize();
	initSegmentPositionFitness<<<blocksPerGrid,threadsPerBlock>>>(segmPosfitness_d,devStates);
	cudaDeviceSynchronize();

	eyeD<<<blocksPerGrid,threadsPerBlock>>>(identitymD,2,numberOfRotationGates);

	/*printf("\nDebug of identities\n");
	 cudaMemcpy(identitym,identitymD,2*2*numberOfRotationGates*sizeof(cuDoubleComplex),cudaMemcpyDeviceToHost);
	 for (int i = 0 ; i < numberOfRotationGates;i++){
	 printm(identitym+2*2*i,2,2,0,NULL);
	 }
	 */
	cudaMemcpy(oldQubits, Qubits, getSizeOfDoubleComplexArrayDevice(Qubits) * sizeof(cuDoubleComplex),
			cudaMemcpyDeviceToDevice);
	cudaMemcpy(oldQutrits, Qutrits, getSizeOfDoubleComplexArrayDevice(Qutrits) * sizeof(cuDoubleComplex),
			cudaMemcpyDeviceToDevice);
	cudaMemset(oldSegmentFitness, 0, getSizeOfFloatArrayDevice(oldSegmentFitness) * sizeof(float));
//	generateSwap(size);
	printf("\nSetup Complete\n");
	//float prevAvSum = avSum;
	srand(time(NULL));
	for (int i = 0; i < maxIterations; i++) {
		//printf("Iteration number %i\n", i);
		debugQubits(debugLevel);
		debugQutrits(debugLevel);
		//	printf("\nIteration number %i:", i);
		int r = rand() % 100;
		//This subroutine is responsible for generation of different random nubmer sequences
		//update_random<< <blocksPerGrid, threadsPerBlock >> >(r, devStates);

		cudaMemset(axis, 0, numberOfRotationGates * sizeof(int));
		measureAxis<<<blocksPerGrid,threadsPerBlock>>>(Qutrits,axis,devStates);
		normalizeAxis<<<blocksPerGrid,threadsPerBlock>>>(axis);

		debugAxis(debugLevel);

//		  This block is to generate a gate after measuring the axis
		constructGates<<<blocksPerGrid,threadsPerBlock>>>(axis,Qubits,gateMatrices,devStates);

		//		  This block is to rearrange matrices for kronecker product
		cudaMemcpy(Data, gateMatrices, sizeOfIndividual * sizeOfPopulation * 4 * sizeof(cuDoubleComplex),
				cudaMemcpyDeviceToDevice);
		int offset = 1;
		int memoryBlockSize = sizeOfPopulation * sizeOfIndividual * 4;
		/*	printf("Total have %i memory cells copied first %i cells for logic gates\n",
		 getSizeOfDoubleComplexArrayDevice(Data), generatedGates * 4);
		 */
		int j = 1;
		while (offset < numberOfWires * numberOfWires) {
			//	printf("offset for %i set of identities is %i, copying %i\n", j, offset * memoryBlockSize,
			//		numberOfWires * memoryBlockSize);

			cudaMemcpy(Data + offset * memoryBlockSize, identitymD,
					numberOfWires * memoryBlockSize * sizeof(cuDoubleComplex), cudaMemcpyDeviceToDevice);
			offset += numberOfWires;

			//	printf("offset for %i set of logic gates is %i, copying %i\n", j + 1, offset * memoryBlockSize,
			//			memoryBlockSize);

			cudaMemcpy(Data + offset * memoryBlockSize, gateMatrices + memoryBlockSize * j,
					memoryBlockSize * sizeof(cuDoubleComplex), cudaMemcpyDeviceToDevice);
			offset += 1;
			j++;
		}

		debugOfRearrangedTemplates(debugLevel, i);

		//		  --End of the block--

		//		  This block performs the expansion by the kronecker product

		cudaMemcpy(Data2, Data, numberOfRotationGates * 4 * sizeof(cuDoubleComplex), cudaMemcpyDeviceToDevice);
		for (int j = 1; j < numberOfWires; j++) {
			int tempSize = (int) pow(2, j);
			int nrows = 2;
			int ncols = 2;
			int offset = 4 * numberOfRotationGates * j;
			int mcols = tempSize;
			int mrows = tempSize;
			kronGPU<<<blocksPerGrid,threadsPerBlock>>>(Data3, Data2, mrows, mcols, Data+offset, nrows,ncols, numberOfRotationGates);
			cudaDeviceSynchronize();
			cuDoubleComplex* t = Data2;
			Data2 = Data3;
			Data3 = t;
		}
		// --End of the block--
		debugExpandedTemplates(debugLevel, i);

		for (int j = 0; j < interactionTemplatesChose; j++) {
			for (int repetitions = 0; repetitions < sizeOfIndividual * sizeOfPopulation; repetitions++) {
				//		printf("I want to copy to %i, whereas the size is: %i , %i elements \n",numberOfRotationGates * size * size + size * size * (j*sizeOfIndividual * sizeOfPopulation+repetitions),getSizeOfDoubleComplexArrayDevice(Data2),size * size);
				//		printf("I want to copy from %i, whereas the size is: %i , %i elements \n",j*size*size,getSizeOfDoubleComplexArrayDevice(interactionMatricesMemoryD),size * size);
				CUDA_CHECK_RETURN(
						cudaMemcpy( Data2 +numberOfRotationGates * size * size + size * size * (j*sizeOfIndividual * sizeOfPopulation+repetitions), interactionMatricesMemoryD+j*size*size, size * size * sizeof(cuDoubleComplex), cudaMemcpyDeviceToDevice));
				//printf("j is %i repetitions is %i offset is %i\n", j,repetitions,(j*sizeOfIndividual * sizeOfPopulation+repetitions));
			}
		}

		cudaMemcpy(Qubits_h + numberOfRotationGates, Qubits + numberOfRotationGates,
				interactionTemplatesNumber * sizeof(cuDoubleComplex), cudaMemcpyDeviceToHost);
		prepareInteractionMatrices(Qubits_h, Data2 + numberOfRotationGates * size * size, size, &handle);
		debugPopulation(debugLevel);

//		 	--End of the block--
		/*
		 printf("\nDebug of SegmentPosFitness before everything\n");
		 CUDA_CHECK_RETURN(cudaMemcpy(segmPosfitness,segmPosfitness_d, (generatedGates*sizeOfIndividual)*sizeof(float),cudaMemcpyDeviceToHost));
		 printfm(segmPosfitness,generatedGates,sizeOfIndividual,0);
		 */

		/*
		 This block finds the sum of rows and columns in the matrix of the fitnesses with respect to position
		 printf("\nsumRowsD size is %i whereas nRows is %i\n", getSizeOfFloatArrayDevice(sumRowsD), generatedGates);
		 printf("sumColsD size is %i whereas nCols is %i\n", getSizeOfFloatArrayDevice(sumColsD), sizeOfIndividual);
		 printf("sumCols size is %i whereas nCols is %i\n", getSizeOfFloatArray(sumCols), sizeOfIndividual);
		 printf("sumRows size is %i whereas nRows is %i\n", getSizeOfFloatArray(sumRows), generatedGates);
		 */
		//	sumRowsAndColumnsGPU(numberOfSegments, sizeOfIndividual, segmPosfitness_d, sumRows, sumCols, sumRowsD, sumColsD,&handle);
		//		debugSegmentPositionFitness(1);
//		--End of the block--
		//0 for no debug, 1 console, 2 to file
//		This block constructs the circuits by selecting segments of which it is composed
//		constructCircuit<<<blocksPerGrid,threadsPerBlock>>>(circuits,segmentHit_d,numberOfSegments,sumColsD, sumRowsD, segmPosfitness_d,devStates);
		cudaMemset(segmentHit_d, 0, getSizeOfIntArrayDevice(segmentHit_d) * sizeof(int));
		cudaMemset(segmentAlreadyUsedD, 0, getSizeOfIntArrayDevice(segmentAlreadyUsedD) * sizeof(int));
		//printf("SegmentAlreadyUsedD size is: %i", getSizeOfIntArrayDevice(segmentAlreadyUsedD));
		//	printf("\n%i\n", getSizeOfIntArrayDevice(segmentAlreadyUsedD));
		//	if (elitism == 1) {
		//	makeEliteCircuit(segmentAlreadyUsedD,circuits,segmentFitness,bsolutID,bsolut);
		//	}

		//To make use of unused segments this loop gets introduced

		//		 This block evaluates fitness
		clearFitness<<<blocksPerGrid,threadsPerBlock>>>(fitness,segmentfitness,segmPosfitness_d,devStates);

		for (subIter = 0; subIter < numberOfSampledCircuits; subIter++) {
			//printf("\tSubiteration number %i\n", subIter);
			constructCircuitWithRepetition<<<blocksPerGrid,threadsPerBlock>>>(circuits,devStates);
			cudaDeviceSynchronize();

			//	CUDA_CHECK_RETURN(
			//		cudaMemcpy(segmentAlreadyUsed, segmentAlreadyUsedD,
			//			getSizeOfIntArrayDevice(segmentAlreadyUsedD) * sizeof(int), cudaMemcpyDeviceToHost));
			//  printim(segmentAlreadyUsed,1,getSizeOfIntArrayDevice(segmentAlreadyUsedD),0,NULL);
			CUDA_CHECK_RETURN(
					cudaMemcpy(circuits_h,circuits,sizeOfIndividual*sizeOfPopulation*sizeof(int),cudaMemcpyDeviceToHost ));
//		--End of the block--

			//0 for no debug, 1 console, 2 to file
			//debugGeneratedCircuits(2);

			/*	printf("\n\nDebug of SegmentHit matrices");
			 cudaMemcpy(segmentHit,segmentHit_d,sizeof(int)*sizeOfIndividual*generatedGates,cudaMemcpyDeviceToHost );
			 printim(segmentHit,generatedGates,sizeOfIndividual,0);
			 */

			//		this block finds the final circuit matrix
			/*printf("Device size is %i, host size is %i, Data size is %i\n", getSizeOfDoubleComplexArray(final_circuit_h),
			 getSizeOfDoubleComplexArrayDevice(final_circuit), getSizeOfDoubleComplexArrayDevice(Data2));
			 */
			cudaDeviceSynchronize();
			for (int j = 0; j < sizeOfPopulation; j++) {
				//	printf("iteration %i %i %i\n",i,final_circuit, Data2);
				CUDA_CHECK_RETURN(
						cudaMemcpy(final_circuit + j * size * size, Data2 + circuits_h[j*sizeOfIndividual]* size * size, size * size * sizeof(cuDoubleComplex), cudaMemcpyDeviceToDevice));
			}
			cudaDeviceSynchronize();
			gpu_blas_mmul(temp_circuit, final_circuit, Data2, circuits_h, size, size, size, handle, size, streams);
			cudaDeviceSynchronize();
//		--End of the block--
			debugFinalCircuitMatrices(debugLevel);
			if (sizeOfIndividual % 2 == 0) {
				cuDoubleComplex* ptr = temp_circuit;
				temp_circuit = final_circuit;
				final_circuit = ptr;
			}

		//	evaluateFitnessGPU<<<blocksPerGrid,threadsPerBlock>>>(final_circuit,target,fitness,size);
			evaluateFitnessAikaterini(final_circuit, target, tempfitness, size, size,size,size, fitness, fitness_h, &handle);
			//	CUDA_CHECK_RETURN(cudaMemcpy(fitness_h,fitness, sizeOfPopulation*sizeof(float),cudaMemcpyDeviceToHost));
//		--End of the block--
			cudaDeviceSynchronize();

	/*		 printf("\nDebug of non inverted Fitness\n");
			 for (int j = 0 ; j < sizeOfPopulation;j++)
			 printf("%f ",fitness_h[j]);
			 printf("\n");
*/

//		This block inverts fintess
//		invertFitness<<<blocksPerGrid,threadsPerBlock>>>(fitness,solut_d);
			//		This block inverts fintess and finds best so far
//      invertFitnessWithBest<<<blocksPerGrid,threadsPerBlock>>>(fitness,solut_d,bsolutD,bsolutID);
			 findBest<<<blocksPerGrid,threadsPerBlock>>>(fitness,solut_d,bsolutD,bsolutID);
			//	thrust::device_ptr<float> d_ptr = thrust::device_pointer_cast(solut_d);
			//  bsolut[0] = *(thrust::max_element(solut_d, solut_d + sizeOfPopulation));
			CUDA_CHECK_RETURN(cudaMemcpy(bsolut, bsolutD, sizeof(float), cudaMemcpyDeviceToHost));

			if (bsolut[0] > oldBSolut) {
				CUDA_CHECK_RETURN(cudaMemcpy(bsolutI, bsolutID, sizeof(int), cudaMemcpyDeviceToHost));
				CUDA_CHECK_RETURN(cudaMemcpy(fitness_h,fitness, sizeOfPopulation*sizeof(float),cudaMemcpyDeviceToHost));
				CUDA_CHECK_RETURN(cudaMemcpy(solut, solut_d, sizeof(int), cudaMemcpyDeviceToHost));
				printInterimResults();
				oldBSolut = *bsolut;
				if (*solut != -1) {
					printf("\nfound a solution!!!\n");
					break;
				}
			}
//		--End of the block--

//		printf("Best solution so far is %f ", *bsolut);
			//	printfm(fitness_h, 1, sizeOfPopulation, 0, NULL);

			//printf("%i %i %i\n",getSizeOfIntArray(segmentHit),getSizeOfIntArrayDevice(segmentHit_d),sizeOfIndividual * (generatedGates * numberOfWires + interactionTemplatesNumber));

			evaluateSegmentFitnessManyPerGen<<<blocksPerGrid,threadsPerBlock>>>(fitness,circuits,segmentfitness);

			CUDA_CHECK_RETURN(
					cudaMemcpy(segmentfitness_h,segmentfitness, numberOfSegments *sizeof(float),cudaMemcpyDeviceToHost));

			//printf("\nDebug of  SegmentFitness\n");
			//printfm(segmentfitness_h, 1, numberOfSegments, 0, NULL);
			CUDA_CHECK_RETURN(
					cudaMemcpy(segmentfitness_h,oldSegmentFitness, numberOfSegments*sizeof(float),cudaMemcpyDeviceToHost));

			if (elitism == 1) {
				//printf("\nDebug of OLD SegmentFitness\n");
				//printfm(segmentfitness_h, 1, numberOfSegments, 0, NULL);
				compareWithPreviuous<<<blocksPerGrid,threadsPerBlock>>>(oldSegmentFitness, segmentfitness, oldQubits, oldQutrits, Qubits, Qutrits,bsolutD);
				CUDA_CHECK_RETURN(
						cudaMemcpy(oldSegmentFitness, segmentfitness,
								numberOfSegments * sizeof(float), cudaMemcpyDeviceToDevice));
				debugQubits(debugLevel);
			}
		}
		if (*solut != -1) {
			printf("\nfound a solution!!!\n");
			break;
		}
		//CUDA_CHECK_RETURN(
		//	cudaMemcpy(segmentfitness_h,segmentfitness, numberOfSegments * sizeof(float),cudaMemcpyDeviceToHost));
		cudaDeviceSynchronize();
		//cublasSasum(handle, numberOfSegments, segmentfitness, 1, &avSum);
		//cudaDeviceSynchronize();
		//
		//	float diff = (avSum - prevAvSum) / numberOfSegments;
		//prevAvSum = avSum;
		//printf("\nDebug of  SegmentFitness:  average fitness is:  %f;improvement is %f\n", avSum / numberOfSegments,
		//		diff);

		int coinToss = rand() % 100;
		if (coinToss < 50) {
			modifyQutritOpers<<<blocksPerGrid,threadsPerBlock>>>(QutritOpers,devStates,segmentfitness);
			constructMatricesForQutritEvolution<<<blocksPerGrid,threadsPerBlock>>>(QutritOpers,MatricesForQutritsEvolution);
			updateQutrits(Qutrits, Qutritst, MatricesForQutritsEvolution, 3, 1, 3);
			cuDoubleComplex* p;
			p = Qutrits;
			Qutrits = Qutritst;
			Qutritst = p;
			//		  --End of the block--
		} else if (coinToss <99)
		{

			updateQubits<<<blocksPerGrid,threadsPerBlock>>>(Qubits,segmentfitness,devStates);
		}
//		  --End of the block--

		/*	printf("\n\nDebug of Qutrits");
		 cudaMemcpy(Qutrits_h,Qutrits,3*generatedGates*numberOfWires*sizeof(cuDoubleComplex),cudaMemcpyDeviceToHost );
		 for (j = 0 ; j < generatedGates*numberOfWires;j++){
		 printm(Qutrits_h+j*1*3,3,1,0);
		 / /printf(" %i \n",axis_h[i]);
		 }*/

	}
	if (*solut == -1)
		*solut = *bsolutI;
	printResults();

	printf("\nSuccessful termination");
	cudaDeviceReset();

	return 0;
}

