/*
 * RevSupport.h
 *
 *  Created on: Dec 15, 2017
 *      Author: root
 */

#include "RevSupport.h"

cuDoubleComplex* parseFile(char* fileName, cuDoubleComplex *target) {
	FILE * pFile;
	size_t len = 0;
	ssize_t read;
	pFile = fopen(fileName, "r");
	char* line = (char *) calloc(1, REV_SUPPORT_BUFFER_SIZE);
	char* endpointer;
	int sizeOfInput = 0;

	if (pFile == NULL) {
		printf("Input file not found, please provide a valid filename as an input argument\n");
		exit(0);
	}

	while ((read = getline(&line, &len, pFile)) != -1) {

		//Set the size of the input
		if (read > 3) {
			if ((line[0] == '.') && (line[1] == 'i') && (line[2] == ' ')) {
				const char s[2] = " ";
				char *token;
				token = strtok(line, s);
				token = strtok(NULL, s);
				sizeOfInput = 1 << atoi(token);

				int i = 0;
				int j = 0;

				if ((1 << numberOfWires) != sizeOfInput) {
					printf(
							"The size of the input of the .pla file does not match the parameter numberOfWires. Please update parameter or change pla file\n");
					exit(0);
				}
				for (i = 0; i < sizeOfInput; i++)
					for (j = 0; j < sizeOfInput; j++) {
						target[i * sizeOfInput + j] = make_cuDoubleComplex(0, 0);
					}
			} else if ((line[0] != '.') && (line[0] != '#')) {

				int inp = (int) strtol(line, &endpointer, 2);
				int out = (int) strtol(endpointer, NULL, 2);
				target[inp * sizeOfInput + out] = make_cuDoubleComplex(1, 0);
			}
		}
	}

	fclose(pFile);
	if (line)
		free(line);
	return target;
}
