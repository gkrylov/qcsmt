/*
 * Rev.Support.h
 *
 *  Created on: Dec 15, 2017
 *      Author: root
 */

#ifndef REV_SUPPORT_H_
#define REV_SUPPORT_H_
#define REV_SUPPORT_BUFFER_SIZE 256
#include "parameters.h"
#include <stdio.h>
#include <stdlib.h>
cuDoubleComplex* parseFile(char* fileName,cuDoubleComplex *target);

#endif /* REV_SUPPORT_H_ */
