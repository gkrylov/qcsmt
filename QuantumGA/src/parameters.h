#ifndef PARAMETERS_H_
#define PARAMETERS_H_

#include <cuComplex.h>
//#include <helper_cuda.h>
#define scaleForRandom 100

#define blocksPerGrid 512
#define threadsPerBlock 512
#define gridSize blocksPerGrid*threadsPerBlock
#define penalty 0.1
#define numberOfMeasurements 50
#define maxIterations 100000000
#define sizeOfPopulation 16
#define numberOfSampledCircuits 16
#define sizeOfIndividual 19
#define interactionTemplatesChose ((numberOfWires-1) * numberOfWires / 2 )
#define debugLevel 0
#define interactionTemplatesNumber ( interactionTemplatesChose * sizeOfIndividual * sizeOfPopulation)
#define numberOfRotationGates (sizeOfPopulation * numberOfWires * sizeOfIndividual)
#define probabilityOfMutation 0.3
#define numberOfWires 3
#define PI 3.14159265358979323846
#define numberOfSegments (interactionTemplatesNumber + numberOfRotationGates)
#include <iostream>
#define mutationRangeCoeff (1<<(numberOfWires))
#define mutationRange (PI/mutationRangeCoeff)
#define elitism 0
#define tol 0.1
typedef struct qutritoper {
	double t1;
	double t2;
	double t3;
	double p1;
	double p2;
	double p3;
	double p4;
	double p5;
} QutritOper;

// 3 qubits for

static void CheckCudaErrorAux(const char *, unsigned, const char *, cudaError_t);
#define CUDA_CHECK_RETURN(value) CheckCudaErrorAux(__FILE__,__LINE__, #value, value)



static void CheckCudaErrorAux(const char *file, unsigned line, const char *statement, cudaError_t err) {
	if (err == cudaSuccess)
		return;
	std::cerr << statement << " returned " << cudaGetErrorString(err) << "(" << err << ") at " << file << ":" << line
			<< std::endl;
	exit(1);
}

#endif /* PARAMETERS_H_ */
